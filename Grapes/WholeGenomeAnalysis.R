library(data.table)

populations <- c("AfricanBeer", "AfricanCocoa", "AfricanPalmWine", "AleBeer", "Alpechin", "AsianRiceFermentation", "Bioethanol", "Dairy", "Ecuadorian",
                 "FarEastAsian", "HumanFrenchGuiana", "ItalianWine1", "MediterraneanOak", "Mezcal", "Malaysian", "NorthAmericanOak",
                 "Sake")

## Read files
read.files <- function(directory) {
  genome.list <- list()
  for (population in populations){
    pop.data <- list()
    files <- list.files(path = paste(directory, "/", population, "/Output/", sep = ""), pattern = "\\.csv$")
    for (file in files){
      output <- read.csv(paste(directory, "/", population, "/Output/", file, sep = ""), header = TRUE)
      gammaexpo <- subset(output, output$model == "GammaExpo")
      gammaexpo$psLps <- gammaexpo$ps/gammaexpo$Lps
      gammaexpo$pnLpn <- gammaexpo$pn/gammaexpo$Lpn
      gammaexpo$pinpis <- gammaexpo$pnLpn/gammaexpo$psLps
      gammaexpo <- gammaexpo[,c("psLps", "pnLpn", "pinpis", "alpha", "omegaA", "obs_dN", "obs_dS", "Lds", "Ldn")]
      gammaexpo$omega <- (gammaexpo$obs_dN/gammaexpo$Ldn)/(gammaexpo$obs_dS/gammaexpo$Lds)
      gammaexpo$omegaNA <- gammaexpo$omega - gammaexpo$omegaA
      gammaexpo$Population <- population
      pop.data[[file]] <- gammaexpo
    }
    genome.list[[population]] <- rbindlist(pop.data)
 }
  genome.full.data <- rbindlist(genome.list)

  #Format data tables
  formatted.popnames <- c("AfricanBeer" = "African Beer",
                          "AfricanCocoa" = "African Cocoa",
                          "AfricanPalmWine" = "African Palm Wine",
                          "AleBeer" = "Ale Beer",
                          "Alpechin" = "Alpechin",
                          "AsianRiceFermentation" = "Asian Rice Fermentation",
                          "Bioethanol" = "Bioethanol",
                          "Dairy" = "Dairy",
                          "Ecuadorian" = "Ecuadorian",
                          "FarEastAsian" = "Far East Asian",
                          "HumanFrenchGuiana" = "Human French Guiana",
                          "ItalianWine1" = "Italian Wine",
                          "Malaysian" = "Malaysian",
                          "MediterraneanOak" = "Mediterranean Oak",
                          "Mezcal" = "Mezcal",
                          "NorthAmericanOak" = "North American Oak",
                          "Sake" = "Sake")
  genome.full.data$Population <- formatted.popnames[genome.full.data$Population]
  #Return results
  return(genome.full.data)
}
genome.full.data<-read.files("BootstrapWholeGenome")
genome.full.datac<-read.files("BootstrapWholeGenomeComplete/")

## Extract statistics

library(plyr)
get.stats <- function(genome.full.data) {
  stats.df <- ddply(.data = genome.full.data,
                    .variables = "Population",
                    .fun = plyr::summarize,
                    Median.Alpha = median(alpha, na.rm = TRUE),
                    Mean.Alpha = mean(alpha, na.rm = TRUE),
                    SD.alpha = sd(alpha, na.rm = TRUE),
                    Median.Omega.A = median(omegaA, na.rm = TRUE),
                    Mean.Omega.A = mean(omegaA, na.rm = TRUE),
                    SD.Omega.A = sd(omegaA, na.rm = TRUE),
                    Median.Omega.NA = median(omegaNA, na.rm = TRUE),
                    Mean.Omega.NA = mean(omegaNA, na.rm = TRUE),
                    SD.Omega.NA = sd(omegaNA, na.rm = TRUE),
                    Median.Omega = median(omega, na.rm = TRUE),
                    Mean.Omega = mean(omega, na.rm = TRUE),
                    SD.Omega = sd(omega, na.rm = TRUE),
                    Median.psLps = median(psLps, na.rm = TRUE),
                    Mean.psLps = mean(psLps, na.rm = TRUE),
                    SD.psLps = sd(psLps, na.rm = TRUE),
                    Median.PiNPiS = median(pinpis, na.rm = TRUE),
                    Mean.PiNPiS = mean(pinpis, na.rm = TRUE),
                    SD.PiNPiS = sd(pinpis, na.rm = TRUE))
  stats.df$LowerCI.Omega.A  <- with(stats.df, Mean.Omega.A  - 1.96*SD.Omega.A)
  stats.df$UpperCI.Omega.A  <- with(stats.df, Mean.Omega.A  + 1.96*SD.Omega.A)
  stats.df$LowerCI.Omega.NA <- with(stats.df, Mean.Omega.NA - 1.96*SD.Omega.NA)
  stats.df$UpperCI.Omega.NA <- with(stats.df, Mean.Omega.NA + 1.96*SD.Omega.NA)
  stats.df$LowerCI.Omega    <- with(stats.df, Mean.Omega    - 1.96*SD.Omega)
  stats.df$UpperCI.Omega    <- with(stats.df, Mean.Omega    + 1.96*SD.Omega)
  return(stats.df)
}
stats.df <- get.stats(genome.full.data)
stats.dfc <- get.stats(genome.full.datac)

write.csv(stats.df, file = "FullTableWholeGenomes.csv", row.names = FALSE)
write.csv(stats.dfc, file = "FullTableWholeGenomesComplete.csv", row.names = FALSE)

#### Figure ####

library(reshape2)
library(ggplot2)
library(ggpubr)
library(ggh4x)
library(gridExtra)

stats.df$LifeHistory <- ifelse(stats.df$Population %in% c("Asian Rice Fermentation", "Sake"), "Asian & Domesticated", ifelse(stats.df$Population %in% c("Malaysian", "Far East Asian", "Ecuadorian", "African Palm Wine", "African Cocoa", "North American Oak"), "Wild", "Non-Asian & Domesticated"))
stats.dfc$LifeHistory <- ifelse(stats.dfc$Population %in% c("Asian Rice Fermentation", "Sake"), "Asian & Domesticated", ifelse(stats.dfc$Population %in% c("Malaysian", "Far East Asian", "Ecuadorian", "African Palm Wine", "African Cocoa", "North American Oak"), "Wild", "Non-Asian & Domesticated"))

# Plot
dummy <- rep(0.1, 26)
stats.df2 <- rbind(dummy, stats.df)

stats.df3 <- melt.data.table(data = as.data.table(stats.df2),
                             measure.vars = patterns("Mean.Omega.[:alpha:]*", "LowerCI.Omega.[:alpha:]*", "UpperCI.Omega.[:alpha:]*"),
                             value.name = c("Mean", "LowerCI", "UpperCI"))
stats.df3 <- as.data.frame(stats.df3)

plt.wg <- ggplot(stats.df3, aes(x = variable, y = Mean)) +
  geom_errorbar(aes(ymin = Mean, ymax = UpperCI, group = variable, color = variable)) +
  geom_col(aes(fill = variable)) +
  facet_nested_wrap(LifeHistory~Population, ncol = 6) +
  ylab(bquote(omega)) +
  scale_x_discrete(labels = expression(omega[a], omega[na])) +
  scale_y_continuous(position = "right") +
  scale_color_manual(values = c("2" = "grey", "1" = "black"), labels = expression(omega[na], omega[a]), name = "") +
  scale_fill_manual(values = c("2" = "grey", "1" = "black"), labels = expression(omega[na], omega[a]), name = "") +
  theme_pubclean() +
  theme(legend.position = c(0.08,0.94),
        strip.background = element_rect(color = "grey", fill = NA),
        axis.title.x = element_blank())

g <- ggplotGrob(plt.wg)
rm_grobs <- g$layout$name %in% c("panel-1", "strip-t-1-1", "strip-t-6-6")
g$grobs[rm_grobs] <- NULL
g$layout <- g$layout[!rm_grobs, ]

# Only the upper strips are framed:
stript <- which(grepl('strip-t', g$layout$name))
stript <- stript[5:length(stript)]
for (i in stript) {
  g$grobs[[i]]$grobs[[1]]$children[[1]]$gp$lty <- 0
}
plot(g)

ggsave(plot = g, file = "WholeGenome.pdf", device = "pdf", height = 8, width = 12)



# Plot all genes
dummy <- rep(0.1, 26)
stats.dfc2 <- rbind(dummy, stats.dfc)

stats.dfc3 <- melt.data.table(data = as.data.table(stats.dfc2),
                              measure.vars = patterns("Mean.Omega.[:alpha:]*", "LowerCI.Omega.[:alpha:]*", "UpperCI.Omega.[:alpha:]*"),
                              value.name = c("Mean", "LowerCI", "UpperCI"))
stats.dfc3 <- as.data.frame(stats.dfc3)

plt.wgc <- ggplot(stats.dfc3, aes(x = variable, y = Mean)) +
  geom_errorbar(aes(ymin = Mean, ymax = UpperCI, group = variable, color = variable)) +
  geom_col(aes(fill = variable)) +
  facet_nested_wrap(LifeHistory~Population, ncol = 6) +
  ylab(bquote(omega)) +
  scale_x_discrete(labels = expression(omega[a], omega[na])) +
  scale_y_continuous(position = "right") +
  scale_color_manual(values = c("2" = "grey", "1" = "black"), labels = expression(omega[na], omega[a]), name = "") +
  scale_fill_manual(values = c("2" = "grey", "1" = "black"), labels = expression(omega[na], omega[a]), name = "") +
  theme_pubclean() +
  theme(legend.position = c(0.08,0.94),
        strip.background = element_rect(color = "grey", fill = NA),
        axis.title.x = element_blank())

g <- ggplotGrob(plt.wgc)
rm_grobs <- g$layout$name %in% c("panel-1", "strip-t-1-1", "strip-t-6-6")
g$grobs[rm_grobs] <- NULL
g$layout <- g$layout[!rm_grobs, ]

# Only the upper strips are framed:
stript <- which(grepl('strip-t', g$layout$name))
stript <- stript[5:length(stript)]
for (i in stript) {
  g$grobs[[i]]$grobs[[1]]$children[[1]]$gp$lty <- 0
}
plot(g)

ggsave(plot = g, file = "WholeGenomeComplete.pdf", device = "pdf", height = 8, width = 12)
