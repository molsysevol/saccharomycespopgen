# Created on 13/02/19 by jdutheil
# Read all outputs from bpppopstats and reconstruct SFS
# Generate input files for Grapes
library(ape)
library(operator.tools)

args <- commandArgs(trailingOnly = TRUE)
population <- args[2]
pop.size <- as.integer(args[3])
nco.group <- args[4]

fams <- read.table("../GeneOverlap/FullOverlapList", sep = "\t")
tbl <- data.frame(Name = substring(fams, 1, nchar(fams) - 6), stringsAsFactors = FALSE)
mito <- as.data.frame(read.gff("./MitoGFF.gff3", na.strings = c(".", "?"), GFF3 = TRUE))
mito <- subset(mito, mito$type == "CDS")
mito$Name <- sapply(strsplit(mito$attributes, ";"), function(x) x[1])
mito$Name <- sapply(strsplit(mito$Name, ":"), function(x) x[2])
tbl <- subset(tbl, tbl$Name %!in% mito$Name)

#Gene subsetting
genesubset <- read.table("../Recombination/RecGenes.csv", header = TRUE, sep = ",")
tbl <- subset(tbl, tbl$Name %in% genesubset$V1)

###
### Unfolded Site Frequency Spectrum
###

get.sfs.grapes.unfolded <- function(genes, nbChr, prefix) {
  n <- length(genes)
  x.name <- character(n)
  x.nchr <- rep(nbChr, n)
  x.lapo <- numeric(n)
  x.lspo <- numeric(n)
  m.sfsa <- matrix(nrow = n, ncol = nbChr - 1)
  m.sfss <- matrix(nrow = n, ncol = nbChr - 1)
  x.ladi <- numeric(n)
  x.lsdi <- numeric(n)
  x.diva <- numeric(n)
  x.divs <- numeric(n)
  pb <- txtProgressBar(1, n, style = 3)
  for (i in 1:n) {
    setTxtProgressBar(pb, i)
    f <- genes[i]
    x.name[i] <- f
    # Parse file 1:
    lines <- readLines(paste(prefix, f, ".popstats.txt", sep = ""));
    if (length(grep("Error", lines)) > 0) {
      x.lapo[i] <- NA
      x.lspo[i] <- NA
      m.sfsa[i,] <- NA
      m.sfss[i,] <- NA
      x.ladi[i] <- NA
      x.lsdi[i] <- NA
      x.diva[i] <- NA
      x.divs[i] <- NA
      cat("Error generated when analysing gene ", f, ".\n", sep = "")
    } else {
      # Get persite calculations:
      sites <- read.table(paste(prefix, f, ".codons.csv", sep = ""), header = TRUE, stringsAsFactors = FALSE, sep = "\t")
      sites <- subset(sites, NCOCategory == nco.group)
      sites.com <- subset(sites, MissingDataFrequency == 0) # Only complete sites
      sites.pol <- subset(sites.com, NbAlleles == 2) # Only biallelic sites
      x.lapo[i] <- sum(3 - sites.com$MeanNumberSynPos)
      x.lspo[i] <- sum(sites.com$MeanNumberSynPos)
      sites.pol <- subset(sites.pol, MinorAllele == OutgroupAllele | MajorAllele == OutgroupAllele) # Only sites for which the outgroup allele is one of the ingroup
      sites.pol$AlleleFrequency <- ifelse(sites.pol$MinorAllele == sites.pol$AncestralAllele, sites.pol$MajorAlleleFrequency, sites.pol$MinorAlleleFrequency)
      # Now get the SFSs:
      sitesS <- subset(sites.pol, IsSynPoly == 1)
      sitesA <- subset(sites.pol, IsSynPoly == 0)
      for (j in 1:(nbChr - 1)) {
        m.sfsa[i, j] <- sum(sitesA$AlleleFrequency == j)
        m.sfss[i, j] <- sum(sitesS$AlleleFrequency == j)
      }
      # Divergence patterns:
      #sites.div <- subset(sites, dN + dS <= 1) # Only one mutation per codon
      x.ladi[i] <- sum(3 - sites$MeanNumberSynPosDiv)
      x.lsdi[i] <- sum(sites$MeanNumberSynPosDiv)
      x.diva[i] <- sum(sites$dN)
      x.divs[i] <- sum(sites$dS)
    }
  }
  dat <- cbind(x.lapo, m.sfsa, x.lspo, m.sfss, x.ladi, x.diva, x.lsdi, x.divs)
  return(dat)
}


write.sfs.grapes.unfolded <- function(sfs, file, nbChr, title, dataset) {
  # Write result in Grapes format:  
  cat(title, "\n#unfolded\n", dataset, "\t", nbChr, "\t", sep = "", file = file)
  cat(colSums(sfs), sep = "\t", file = file, append = TRUE)
  cat("\n", file = file, append = TRUE)
}


sfs <- get.sfs.grapes.unfolded(tbl$Name, 
                               nbChr = pop.size,
                               prefix = paste("../PopStats/", population, "/CodonStats/", sep = ""))

bootstrap <- function(sfs, nboots, prefix) {
  for (i in 1:nboots) {
    sfs.rep <- sfs[sample.int(nrow(sfs), replace = TRUE), ]
    write.sfs.grapes.unfolded(sfs.rep, nbChr = pop.size,
                              paste("RecNCOCategories/", population, "/", nco.group, "/SFSs/", prefix, "_rep", i, ".sfs", sep = ""),
                              title = population,
                              dataset = paste(prefix, "_rep", i, sep = ""))
  }
}

bootstrap(sfs, 150, prefix = population)
