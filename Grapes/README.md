# Inferring rates of adaptive protein evolution using grapes
# Created on 31/12/2021 by Maximilian Raas

#### Create directory system:
```
./mkdirs.sh
```
### Whole Genomes

#### Create bootstrapped SFSs from whole genomes:
```
./SFSMakerCommandsBootstrapWholeGenome.sh
```

#### Run grapes on bootstrapped SFSs from whole genomes:
```
./GrapesCommandsBootstrapWholeGenome.sh
```

#### Create bootstrapped SFSs from whole genomes, using all available genes for each species:
```
./SFSMakerCommandsBootstrapWholeGenomeComplete.sh
```

#### Run grapes on bootstrapped SFSs from whole genomes, using all available genes for each species:
```
./GrapesCommandsBootstrapWholeGenomeComplete.sh
```



### Gene sets

#### Create bootstrapped SFSs from gene sets:
```
./SFSMakerCommandsBootstrapGeneSets.sh
```

#### Run grapes on bootstrapped SFSs from gene sets:
```
./GrapesCommandsBootstrapGeneSets.sh
```

### Recombination windows

#### Create bootstrapped SFSs from recombination windows:

Crossover rates
```
./SFSMakerCommandsRecCOCategories.sh
```
Noncrossover rates
```
./SFSMakerCommandsRecNCOCategories.sh
```

#### Run grapes on bootstrapped SFSs from recombination windows:

Crossover rates
```
./GrapesCommandsBootstrapRecCOCategories.sh
```

Noncrossover rates
```
./GrapesCommandsBootstrapRecNCOCategories.sh
```

### Recombination windows, using synonymous site for the highest category only

#### Create bootstrapped SFSs from recombination windows:

Crossover rates
```
./SFSMakerCommandsRecCOCategoriesCodonBias.sh
```

#### Run grapes on bootstrapped SFSs from recombination windows:

Crossover rates
```
./GrapesCommandsBootstrapRecCOCategoriesCodonBias.sh
```


### RSA categories

#### Create bootstrapped SFSs from RSA categories:
```
./SFSMakerCommandsRSACategories.sh
```

#### Run grapes on bootstrapped SFSs from RSA categories:
```
./GrapesCommandsBootstrapRSACategories.sh
```

### RSA categories synonymous sites only from RSA category 10 (i.e. most exposed sites) to account for more prevalent codon bias at buried sites

#### Create bootstrapped SFSs from RSA categories with synonymous sites exclusively from category 10:
```
./SFSMakerCommandsRSACategoriesCodonBias.sh
```

#### Run grapes on bootstrapped SFSs from RSA categories with synonymous sites exclusively from category 10:
```
./GrapesCommandsBootstrapRSACategoriesCodonBias.sh
```

#### Grapes output is stored as gzipped tar archives. To access these use:

Bootstrap Gene Sets
```
tar xvzf BootstrapGeneSets.tar.gz
```

Bootstrap Whole Genome
```
tar xvzf Bootstrap.tar.gz
```

Bootstrap Recombination CO Categories
```
tar xvzf BootstrapRecCOCategories.tar.gz
```

Bootstrap Recombination NCO Categories
```
tar xvzf BootstrapRecNCOCategories.tar.gz
```

Bootstrap RSA Categories
```
tar xvzf BootstrapRSACategories.tar.gz
```

Bootstrap RSA Categories Codon Bias
```
tar xvzf BootstrapRSACategoriesCodonBias.tar.gz
```
