from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
import glob

seqfiles = list(glob.glob("../AlignmentProcessing/AfricanBeer/R64ungapped/*"))

with open("../GeneOverlap/FullOverlapList", "r") as f:
    overlapgenes = list()
    for line in f:
        overlapgenes.append(line)

overlapgenes = [x.strip() for x in overlapgenes]

for genefile in seqfiles:
    gname = genefile.split("/")[-1].split(".")[0]
    if gname in overlapgenes:
        parsedgene = SeqIO.parse(genefile, "fasta")
        for seq_record in parsedgene:
            aaseq = seq_record.seq.translate()
            aaseqrecord = SeqRecord(aaseq, id=gname)
            aaseqrecord.description = ""
            outfile = "R64seqs/" + gname + ".fasta"
            SeqIO.write(aaseqrecord, outfile, "fasta")
