# Predicting solvent accessibility with NetSurfP-2.0
# Created on 22/12/2021 by Maximilian Raas

#### Prepare directories:
```
./mkdirs.sh
```

#### Prepare R64 sequences:

Only genes recovered from all lineages are used
Create amino acid sequences of the R64 reference isolate

```
python3 ExtractR64.py
```

Note: Ungapped R64 sequences are taken from the African Beer lineage. This should be representative for all R64 of the overlapping gene set.

#### Run NetSurfP-2.0 on all R64 sequences from the overlapping gene set:

```
./NetSurfP2.sh
```

The output of NetsurfP-2.0 after running these commands is stored in directory "R64output" in tar.gz file archive as "NetSurfP2Output.csv.tar.gz".

#### Get corresponding sequence indices for R64 sites in lineage-specific alignments:

Run sged-create-sequence-index.py for all genes in all populations
```
./SeqIndexCommands.sh
```

Add index information into codon statistics files
```
Rscript CatStatIndex.R
```

#### Add RSA information into codon statistics files:
```
Rscript CatRSAInfo.R
```

#### Bin sites into 15 categories based on RSA:
```
Rscript RSACat.R
```
