mkdir R64seqs
mkdir R64out
mkdir SeqIndex
POPS=(AfricanBeer AfricanCocoa AfricanPalmWine AleBeer Alpechin AsianRiceFermentation Bioethanol Dairy Ecuadorian FarEastAsian HumanFrenchGuiana ItalianWine1 ItalianWine2 MediterraneanOak Mezcal Malaysian NorthAmericanOa Sake)
for pop in "${POPS[@]}"
do
 mkdir SeqIndex/$pop
done
