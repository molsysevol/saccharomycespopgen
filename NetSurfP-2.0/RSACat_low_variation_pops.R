library(data.table)
library(ggplot2)

populations <- c("Ecuadorian", "Malaysian")

overlaps <- read.table("../GeneOverlap/FullOverlapList", header = FALSE, sep = "\t")

all.sites.tables <- list()
for (population in populations){
  population.sites.tables <- list()
  bpps <- Sys.glob(paste("../PopStats/", population, "/CodonStats/*.codons.csv", sep = ""))
  for (gfile in bpps){
    gene <- as.character(lapply(strsplit(gfile, split = "/"), function(x) x[[5]]))
    gene <- as.character(lapply(strsplit(gene, split = "\\."), function(x) x[[1]]))
    if (gene %in% overlaps$V1){
      bpp <- read.table(gfile, sep = "\t", header = TRUE)
      drop = c("RSA.group", "COCategory", "NCOCategory", "CORate", "NCORate")
      bpp <- bpp[,!(names(bpp) %in% drop)]
      bpp$Gene <- gene
      bpp$Population <- population
      population.sites.tables[[gene]] <- bpp
    }
  }
  population.sites <- rbindlist(population.sites.tables, use.names = TRUE)
  all.sites.tables[[population]] <- population.sites
}

all.sites <- rbindlist(all.sites.tables)
all.sites$RSA.group <- as.numeric(cut_number(all.sites$RSA, 5))

for (population in populations){
  pop.data <- subset(all.sites, Population == population)
  for (gene in unique(pop.data$Gene)){
    gbpp <- subset(pop.data, Gene == gene)
    write.table(gbpp, paste("../PopStats/", population, "/CodonStats/", gene, ".codons.csv", sep = ""), sep = "\t", row.names = FALSE)
  }
}
