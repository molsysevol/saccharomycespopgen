import glob
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--population", type=str)
population = parser.parse_args()
population = str(vars(population))
population = population.split("'")[3]

commands = list()

alngenes = list(glob.glob("../OutgroupMatching/{}/BlastOutSeqs/Aligned/*.fasta".format(population)))
for gene in alngenes:
    	gene = gene.split("/")[-1].split(".")[0]
    	command = "bpppopstats param=PopStatsEstimateKappa.bpp DIR=../OutgroupMatching/{}/BlastOutSeqs/Aligned/ OUT=./{}/KappaEstimates/ DATA=".format(population, population) + gene
    	commandslist.append(command)

with open("PopStatsKappaEstCommands{}.sh".format(population), "w") as f:
    for item in commandslist:
        f.write("%s\n" % item)
