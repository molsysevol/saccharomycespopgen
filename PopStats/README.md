# Compute codon-level statistics on diversity and divergence
# Created on 19/12/2021 by Maximilian Raas

#### Create directory system:
```
./mkdirs.sh
```

#### Generate commands to estimate transition/transversion ratio (kappa) across lineages:

African Beer
```
python3 PopStatsKappaEstCommands.py -p AfricanBeer
```
African Cocoa
```
python3 PopStatsKappaEstCommands.py -p AfricanCocoa
```

African Palm Wine
```
python3 PopStatsKappaEstCommands.py -p AfricanPalmWine
```

Ale Beer
```
python3 PopStatsKappaEstCommands.py -p AleBeer
```

Alpechin
```
python3 PopStatsKappaEstCommands.py -p Alpechin
```

Asian Rice Fermentation
```
python3 PopStatsKappaEstCommands.py -p AsianRiceFermentation
```

Bioethanol
```
python3 PopStatsKappaEstCommands.py -p Bioethanol
```

Dairy
```
python3 PopStatsKappaEstCommands.py -p Dairy
```

Ecuadorian
```
python3 PopStatsKappaEstCommands.py -p Ecuadorian
```

Far East Asian
```
python3 PopStatsKappaEstCommands.py -p FarEastAsian
```

Human French Guiana
```
python3 PopStatsKappaEstCommands.py -p HumanFrenchGuiana
```

Italian Wine 1
```
python3 PopStatsKappaEstCommands.py -p ItalianWine1
```

Italian Wine 2
```
python3 PopStatsKappaEstCommands.py -p ItalianWine2
```

Malaysian
```
python3 PopStatsKappaEstCommands.py -p Malaysian
```

Mediterranean Oak
```
python3 PopStatsKappaEstCommands.py -p MediterraneanOak
```

Mezcal
```
python3 PopStatsKappaEstCommands.py -p Mezcal
```

North American Oak
```
python3 PopStatsKappaEstCommands.py -p NorthAmericanOak
```

Sake
```
python3 PopStatsKappaEstCommands.py -p Sake
```

#### Run kappa estimation commands:

African Beer
```
./PopStatsKappaEstCommandsAfricanBeer.sh
```

African Cocoa
```
./PopStatsKappaEstCommandsAfricanCocoa.sh
```

African Palm Wine
```
./PopStatsKappaEstCommandsAfricanPalmWine.sh
```

Ale Beer
```
./PopStatsKappaEstCommandsAleBeer.sh
```

Asian Rice Fermentation
```
./PopStatsKappaEstCommandsAsianRiceFermentation.sh
```

Bioethanol
```
./PopStatsKappaEstCommandsBioethanol.sh
```

Dairy
```
./PopStatsKappaEstCommandsDairy.sh
```

Ecuadorian
```
./PopStatsKappaEstCommandsEcuadorian.sh
```

Far East Asian
```
./PopStatsKappaEstCommandsFarEastAsian.sh
```

Human French Guiana
```
./PopStatsKappaEstCommandsHumanFrenchGuiana.sh
```

Italian Wine 1
```
./PopStatsKappaEstCommandsItalianWine1.sh
```

Italian Wine 2
```
./PopStatsKappaEstCommandsItalianWine2.sh
```

Malaysian
```
./PopStatsKappaEstCommandsMalaysian.sh
```

Mediterranean Oak
```
./PopStatsKappaEstCommandsMediterraneanOak.sh
```

Mezcal
```
./PopStatsKappaEstCommandsMezcal.sh
```

North American Oak
```
./PopStatsKappaEstCommandsNorthAmericanOak.sh
```

Sake
```
./PopStatsKappaEstCommandsSake.sh
```

#### Analyse kappa estimate results:

African Beer
```
Rscript KappaEstimation.R --args AfricanBeer
```

African Cocoa
```
Rscript KappaEstimation.R --args AfricanCocoa
```

African Palm Wine
```
Rscript KappaEstimation.R --args AfricanPalmWine
```

Ale Beer
```
Rscript KappaEstimation.R --args AleBeer
```

Alpechin
```
Rscript KappaEstimation.R --args Alpechin
```

Asian Rice Fermentation
```
Rscript KappaEstimation.R --args AsianRiceFermentation
```

Bioethanol
```
Rscript KappaEstimation.R --args Bioethanol
```

Dairy
```
Rscript KappaEstimation.R --args Dairy
```

Ecuadorian
```
Rscript KappaEstimation.R --args Ecuadorian
```

Far East Asian
```
Rscript KappaEstimation.R --args FarEastAsian
```

Human French Guiana
```
Rscript KappaEstimation.R --args HumanFrenchGuiana
```

Italian Wine 1
```
Rscript KappaEstimation.R --args ItalianWine1
```

Italian Wine 2
```
Rscript KappaEstimation.R --args ItalianWine2
```

Malaysian
```
Rscript KappaEstimation.R --args Malaysian
```

Mediterranean Oak
```
Rscript KappaEstimation.R --args MediterraneanOak
```

Mezcal
```
Rscript KappaEstimation.R --args Mezcal
```

North American Oak
```
Rscript KappaEstimation.R --args NorthAmericanOak
```

Sake
```
Rscript KappaEstimation.R --args Sake
```

#### Kappa estimates are all ~4.5 (mean 4.52, SD = 0.02)


#### Generate PopStats commands to calculate codon-level statistics:

African Beer
```
python3 PopStatsCommands.py -p AfricanBeer
```

African Cococa
```
python3 PopStatsCommands.py -p AfricanCocoa
```

African Palm Wine
```
python3 PopStatsCommands.py -p AfricanPalmWine
```

Ale Beer
```
python3 PopStatsCommands.py -p AleBeer
```

Alpechin
```
python3 PopStatsCommands.py -p Alpechin
```

Asian Rice Fermentation
```
python3 PopStatsCommands.py -p AsianRiceFermentation
```

Bioethanol
```
python3 PopStatsCommands.py -p Bioethanol
```

Dairy
```
python3 PopStatsCommands.py -p Dairy
```

Ecuadorian
```
python3 PopStatsCommands.py -p Ecuadorian
```

Far East Asian
```
python3 PopStatsCommands.py -p FarEastAsian
```

Human French Guiana
```
python3 PopStatsCommands.py -p HumanFrenchGuiana
```

Italian Wine 1
```
python3 PopStatsCommands.py -p ItalianWine1
```

Italian Wine 2
```
python3 PopStatsCommands.py -p ItalianWine2
```

Malaysian
```
python3 PopStatsCommands.py -p Malaysian
```

Mediterranean Oak
```
python3 PopStatsCommands.py -p MediterraneanOak
```

Mezcal
```
python3 PopStatsCommands.py -p Mezcal
```

North American Oak
```
python3 PopStatsCommands.py -p NorthAmericanOak
```

Sake
```
python3 PopStatsCommands.py -p Sake
```

#### Run PopStats commands:

African Beer
```
./PopStatsCommandsAfricanBeer.sh
```

African Cocoa
```
./PopStatsCommandsAfricanCocoa.sh
```

African Palm Wine
```
./PopStatsCommandsAfricanPalmWine.sh
```

Ale Beer
```
./PopStatsCommandsAleBeer.sh
```

Alpechin
```
./PopStatsCommandsAlpechin.sh
```

Asian Rice Fermentation
```
./PopStatsCommandsAsianRiceFermentation.sh
```

Bioethanol
```
./PopStatsCommandsBioethanol.sh
```

Dairy
```
./PopStatsCommandsDairy.sh
```

Ecuadorian
```
./PopStatsCommandsEcuadorian.sh
```

Far East Asian
```
./PopStatsCommandsFarEastAsian.sh
```

Human French Guiana
```
./PopStatsCommandsHumanFrenchGuiana.sh
```

Italian Wine 1
```
./PopStatsCommandsItalianWine1.sh
```

Italian Wine 2
```
./PopStatsCommandsItalianWine2.sh
```

Malaysian
```
./PopStatsCommandsMalaysian.sh
```

Mediterranean Oak
```
./PopStatsCommandsMediterraneanOak.sh
```

Mezcal
```
./PopStatsCommandsMezcal.sh
```

North American Oak
```
./PopStatsCommandsNorthAmericanOak.sh
```

Sake
```
./PopStatsCommandsSake.sh
```

#### Popstats output files are stored as gzipped tar archives. To access these use:
```
extract_CodonStats() {
  cd $1/CodonStats/
  tar xvzf ${1}CodonStats.tar.gz
  cd ../..
}
extract_CodonStats AfricanBeer
extract_CodonStats AfricanCocoa
extract_CodonStats AfricanPalmWine
extract_CodonStats AleBeer
extract_CodonStats Alpechin
extract_CodonStats AsianRiceFermentation
extract_CodonStats Bioethanol
extract_CodonStats Dairy
extract_CodonStats Ecuadorian
extract_CodonStats FarEastAsian
extract_CodonStats HumanFrenchGuiana
extract_CodonStats ItalianWine1
extract_CodonStats ItalianWine2
extract_CodonStats Malaysian
extract_CodonStats MediterraneanOak
extract_CodonStats Mezcal
extract_CodonStats NorthAmericanOak
extract_CodonStats Sake
```
