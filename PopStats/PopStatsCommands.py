import glob
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--population", type=str)
population = parser.parse_args()
population = str(vars(population))
population = population.split("'")[3]

commandslist = list()

alngenes = list(glob.glob("../OutgroupMatching/{}/BlastOutSeqs/Aligned_NT/*.fasta".format(population)))
for gene in alngenes:
	gene = gene.split("/")[-1].split(".")[0]
	command = "bpppopstats param=PopStats.bpp DIR=../OutgroupMatching/{}/BlastOutSeqs/Aligned_NT/ OUT=./{}/CodonStats/ DATA=".format(population, population) + gene
	commandslist.append(command)

with open("PopStatsCommands{}.sh".format(population), "w") as f:
    for item in commandslist:
        f.write("%s\n" % item)
