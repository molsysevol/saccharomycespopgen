library(data.table)
library(ape)
library(ggplot2)

populations <- c("Malaysian", "Ecuadorian", "NorthAmericanOak")

chromosomes <- c(1:16)
chromosomes <- as.roman(chromosomes)
chromosomes <- as.character(chromosomes)

gff <- read.gff("./R64.gff3", na.strings = c(".", "?"), GFF3 = TRUE)
gff <- subset(gff, seqid != "Mito")
genes <- subset(gff, gff$type == "gene")
ids <- sapply(strsplit(genes$attributes, ";"), function(x) x[1])
gnames <- sapply(strsplit(ids, ":"), function(x) x[2])
genes$GeneName <- gnames
genes$seqid <- as.character(genes$seqid)
overlappinggenes <- read.table("../GeneOverlap/FullOverlapList")
overlappinggenes <- as.vector(overlappinggenes$V1)
genes <- subset(genes, GeneName %in% overlappinggenes)

i = 0

geneslist <- list()

for (chromosome in chromosomes){
	i = i + 1
	chromdata <- subset(genes, seqid == chromosome)
	chromdata$ChromosomeNumber <- i
	geneslist[[chromosome]] <- chromdata
}

genes <- rbindlist(geneslist)
genesinfo <- genes[, c(4,5,10,11)]

RecMap <- read.csv("RecombinationMap.csv")

genesinfolist <- list()

for (i in 1:16){
	chr.genes <- subset(genesinfo, ChromosomeNumber == i)
	chr.CO <- subset(RecMap, chr == i)
	for (j in 1:nrow(chr.genes)){
		geneinfo <- chr.genes[j, ]
		geneinfo$Midpoint <- (geneinfo$start + geneinfo$end)/2
		gname <- geneinfo$GeneName
		for (k in 1:nrow(chr.CO)){
			windowinfo <- chr.CO[k, ]
			if (geneinfo$Midpoint >= windowinfo$cer_start && geneinfo$Midpoint <= windowinfo$cer_end){
				geneinfo$CO.rate <- windowinfo$cer_co_rate
				geneinfo$NCO.rate <- windowinfo$cer_nco_rate
				genesinfolist[[gname]] <- geneinfo
			}
		}
	}
}

genesinfo <- rbindlist(genesinfolist)
rec.genes <- data.frame(V1 = genesinfo$GeneName)
write.table(rec.genes, "RecGenes.csv", sep = ",", row.names = FALSE)

genesinfo$COCategory <- as.numeric(cut_number(genesinfo$CO.rate, 5))
genesinfo$NCOCategory <- as.numeric(cut_number(genesinfo$NCO.rate, 5))

for (gname in genesinfo$GeneName){
	geneinfo <- genesinfo[GeneName == gname]
	CORate <- geneinfo$CO.rate
	NCORate <- geneinfo$NCO.rate
	COCategory <- geneinfo$COCategory
	NCOCategory <- geneinfo$NCOCategory
	for (population in populations){
		bpp <- read.table(paste("../PopStats/", population, "/CodonStats/", gname, ".codons.csv", sep = ""), sep = "\t", header = TRUE)
		bpp$COCategory <- COCategory
		bpp$NCOCategory < NCOCategory
		bpp$CORate <- CORate
		bpp$NCORate <- NCORate
		write.table(bpp, paste("../PopStats/", population, "/CodonStats/", gname, ".codons.csv", sep = ""), sep = "\t", row.names = FALSE)
	}
}
