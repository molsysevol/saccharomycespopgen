# Recombination information across genes
# Created on 24/12/2021 by Maximilian Raas

The _S. cerevisiae_ recombination map was kindly provided by Haoxuan Liu, see https://doi.org/10.1093/molbev/msy233

Apply recombination categories to genes by midpoint coordinate matching to map window

```
Rscript RecMap2Genes_high_variation_pops.R
Rscript RecMap2Genes_low_variation_pops.R
```
