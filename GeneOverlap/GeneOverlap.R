library(UpSetR)

populations <- list("AfricanBeer", "AfricanCocoa", "AfricanPalmWine", "AleBeer", "Alpechin", "AsianRiceFermentation", "Bioethanol", "Dairy", "Ecuadorian", 
                    "FarEastAsian", "HumanFrenchGuiana", "ItalianWine1", "ItalianWine2", "Malaysian", "MediterraneanOak", "Mezcal", "NorthAmericanOak", "Sake")

pop.genes.list <- list()
for (population in populations){
	pop.genes <- data.frame(Genes = list.files(path = paste("../OutgroupMatching/", population, "/BlastOutSeqs/Aligned_NT/", sep = ""), pattern = "\\.fasta$"))
	pop.genes$Genes <- sapply(strsplit(as.character(pop.genes$Genes), "\\."), function(x) x[1])
	pop.genes.list[[population]] <- pop.genes
}

list2env(pop.genes.list, envir=.GlobalEnv)

overlappinggeneslist <- Reduce(merge, list(AfricanBeer, AfricanCocoa, AfricanPalmWine, AleBeer, Alpechin, AsianRiceFermentation, Bioethanol, Dairy, Ecuadorian, FarEastAsian, HumanFrenchGuiana, ItalianWine1, ItalianWine2, Malaysian, MediterraneanOak, Mezcal, NorthAmericanOak, Sake))
write.table(overlappinggeneslist, "FullOverlapList", row.names = FALSE, col.names = FALSE, quote = FALSE)


AfricanBeervec <- unlist(AfricanBeer)
AfricanCocoavec <- unlist(AfricanCocoa)
AfricanPalmWinevec <- unlist(AfricanPalmWine)
AleBeervec <- unlist(AleBeer)
Alpechinvec <- unlist(Alpechin)
AsianRiceFermentationvec <- unlist(AsianRiceFermentation)
Bioethanolvec <- unlist(Bioethanol)
Dairyvec <- unlist(Dairy)
Ecuadorianvec <- unlist(Ecuadorian)
FarEastAsianvec <- unlist(FarEastAsian)
HumanFrenchGuianavec <- unlist(HumanFrenchGuiana)
ItalianWine1vec <- unlist(ItalianWine1)
ItalianWine2vec <- unlist(ItalianWine2)
Malaysianvec <- unlist(Malaysian)
MediterraneanOakvec <- unlist(MediterraneanOak)
Mezcalvec <- unlist(Mezcal)
NorthAmericanOakvec <- unlist(NorthAmericanOak)
Sakevec <- unlist(Sake)

listInput <- list(`African Beer` = AfricanBeervec, `African Cocoa` = AfricanCocoavec, `African Palm Wine` = AfricanPalmWinevec, `Ale Beer` = AleBeervec, Alpechin = Alpechinvec,
                  `Asian Rice Fermentation` = AsianRiceFermentationvec, Bioethanol = Bioethanolvec, Dairy = Dairyvec, Ecuadorian = Ecuadorianvec, 
                  `Far East Asian` = FarEastAsianvec, `Human French Guiana` = HumanFrenchGuianavec, `Italian Wine 1` = ItalianWine1vec, `Italian Wine 2` = ItalianWine2vec, 
                  Malaysian = Malaysianvec, `Mediterranean Oak` = MediterraneanOakvec, Mezcal = Mezcalvec, 
                  `North American Oak` = NorthAmericanOakvec, `Sake` = Sakevec)

pdf("UpSet.pdf")
upset(fromList(listInput), order.by = "freq", nsets = 18)
dev.off()
