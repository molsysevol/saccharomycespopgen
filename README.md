# SaccharomycesPopGen
# Created on 8/12/21 by Maximilian Raas & Julien Dutheil

Scripts and data necessary to reproduce the analyses in *The rate of adaptive molecular evolution in wild and domesticated Saccharomyces cerevisiae populations*, by Maximilian W.D. Raas and Julien Y. Dutheil (preprint available at https://doi.org/10.1101/2022.12.07.519429).

#### Genome alignments
See the `GenomeAlignments` subdirectory

#### Alignment processing
See the `AlignmentProcessing` subdirectory

#### Outgroup matching
See the `OutgroupMatching` subdirectory

#### Phylogenetics
See the `Phylogenetics` subdirectory

#### Gene overlap
See the `GeneOverlap` subdirectory

#### Gene sets
See the `GeneSets` subdirectory

#### Computation of per gene statistics
See the `PopStats` subdirectory

Generates the SFS to serve as input for Grapes, as well as PiN/PiS statistics.

#### Determine RSA
See the `NetSurfP-2.0` subdirectory

#### Recombination
See the `Recombination` subdirectory

#### Demographic history
See the `MSMC` subdirectory

#### Adaptive evolution inference
See the `Grapes` subdirectory

#### Codon usage analysis
See the `CodonUsage` subdirectory

#### LD decay analyses
See the `LD` subdirectory

#### Simulations
See the `Simulations` subdirectory
