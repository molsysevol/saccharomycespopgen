#! /usr/bin/python
# Concatenate alignments, output breakpoints and ordered list of sequence ids.

from Bio import AlignIO
from Bio import SeqIO
from os import listdir
from os.path import join
import sys

def are_seq_analyzable(s1, s2) :
    foundHom = False
    foundHet = False
    chars = ["A", "C", "G", "T"]
    for i in range(len(s1)) :
        if s1[i] in chars and s2[i] in chars :
            if s1[i] == s2[i] :
                foundHom = True
            if s1[i] != s2[i] :
                foundHet = True
        if foundHom and foundHet:
            return True
    return False


if __name__ == "__main__":
    population = sys.argv[1]

    breakpoints = open("Output/%s/ismc_breakpoints.txt" % population, 'w')
    sequenceids = open("Output/%s/ismc_sequenceids.txt" % population, 'w')
    inputaligns = open("Output/%s/ismc_alignments.txt" % population, 'w')
    ignoreSequences = []

    files = [f for f in listdir("Alignments/%s" % (population)) if f.endswith(".fasta")]
    files = sorted(files)
    for f in files:
        inputaligns.write("%s\n" % f)

    aln = AlignIO.read("Alignments/%s/%s" % (population, files.pop(0)), "fasta")
    aln.sort()
    #check if some sequences are identical:
    for i in range(len(aln)-1):
        for j in range(i+1, len(aln)):
            #print("%i %i" % (i, j))
            if (not are_seq_analyzable(aln[i].seq, aln[j].seq)) :
                if (not aln[j].id in ignoreSequences) :
                    print("Identical sequences found, sequence %s will be ignored." % aln[j].id)
                    ignoreSequences.append(aln[j].id)


    breakpoints.write("%i,%i\n" % (1, len(aln[0].seq)))
    for file in files:
        tmp = AlignIO.read("Alignments/%s/%s" % (population, file), "fasta")
        tmp.sort()
        a = len(aln[0].seq)
        aln = aln + tmp
        b = len(aln[0].seq)
        breakpoints.write("%i,%i\n" % (a + 1, b))
    
        #check if some sequences are identical:
        for i in range(len(tmp)-1):
            for j in range(i+1, len(tmp)):
                #print("%i %i" % (i, j))
                if (not are_seq_analyzable(tmp[i].seq, tmp[j].seq)) :
                    if (not tmp[j].id in ignoreSequences) :
                        print("Identical sequences found, sequence %s will be ignored." % tmp[j].id)
                        ignoreSequences.append(tmp[j].id)

    aln2 = [r for r in aln if not r.id in ignoreSequences]
    print("%i sequences discarded." % len(ignoreSequences))

    SeqIO.write(aln2, "Output/%s/ismc_alignment.fasta" % population, "fasta")
    for record in aln:
        if not record.id in ignoreSequences:
          sequenceids.write(record.id + "\n")

    breakpoints.close()
    sequenceids.close()
    inputaligns.close()

    print("Done.")

