# Created on 20.09.22

library(data.table)

info<-read.csv("../IsolatesInfo.csv")
infoh <- subset(info, Zygosity == "homozygous") #121 strains
lst<-split(infoh$Assembly_IDs, infoh$Population)
len<-sapply(lst, length)
len<-len[len>1]

populations <- names(len)

# Homogeneous estimators:
dat.lst <- list()
for (pop in populations) {
  p <- gsub(" ", "", pop)
  try({ param <- read.table(paste0("Output/", p, "/", p, "_estimates.txt"), sep = " ", skip = 1, row.names = 1);
        dat.lst[[pop]] <- data.frame(Population = pop, Rho = param["rho", 1], Theta = param["theta", 1])
        })
}
dat <- rbindlist(dat.lst)

# Non homogeneous estimators:
dat.lst <- list()
for (pop in populations) {
  p <- gsub(" ", "", pop)
  try({ param <- read.table(paste0("Output5rc/", p, "/", p, "_estimates.txt"), sep = " ", skip = 1, row.names = 1);
        dat.lst[[pop]] <- data.frame(Population = pop, Rho = param["rho", 1], Theta = param["theta", 1])
        })
}
datnh <- rbindlist(dat.lst)


library(ggplot2)
ggplot(dat, aes(x = Theta, y = Rho)) + geom_label(aes(label=Population))
mean(dat$Rho) #4e-3
dat$RhoOverTheta <- dat$Rho / dat$Theta
dat$Population <- factor(dat$Population, levels = dat$Population[order(dat$RhoOverTheta, decreasing = FALSE)])
ggplot(dat[order(Rho/Theta),], aes(x = Rho/Theta, y = Population)) + geom_point() 

write.csv(dat, file = "iSMCestimates.csv", row.names = FALSE)

