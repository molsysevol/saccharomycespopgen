# iSMC analyses

Prepare the input files
=======================

```bash
mkdir Alignments
mkdir Statistics
mkdir Log
mkdir Output
maffilter=maffilter131
```

We only select isolates that are haploid or homozygous polyploid, so that de novo assembled genomes represent "true" haplotypes:

```r
info<-read.csv("../IsolatesInfo.csv")
infoh <- subset(info, Zygosity == "homozygous") #121 strains
lst<-split(infoh$Assembly_IDs, infoh$Population)
len<-sapply(lst, length)
len<-len[len>1]
```

this leads to 10 populations (Italian wine being split in two):

```
"African Palm Wine"  "Alpechin"           "Bioethanol"        
"Ecuadorian"         "Far East Asian"     "Italian Wine 1"    
"Italian Wine 2"     "Malaysian"          "Mediterranean Oak" 
"North American Oak" "Sake"         
```

```r
unlink("runMaffilter.sh")
for (pop in names(len)) {
  p <- gsub(" ", "", pop)
  cat("mkdir -p Alignments/", p, "; maffilter param=MafFilter-iSMC.bpp ISOLATES=\"(", paste(lst[[pop]], collapse=","), ")\" POPULATION=", p, "\n", sep = "",
      file = "runMaffilter.sh",
      append = TRUE)
}
```

Concatenate all alignments and generate breakpoints:

```r
unlink("runFormatSeq.sh")
for (pop in names(len)) {
  p <- gsub(" ", "", pop)
  cat("mkdir -p Output/", p, "; python3 formatSeqs.py ", p, "\n", sep = "",
      file = "runFormatSeq.sh",
      append = TRUE)
}
```

```
0 sequences discarded.
Done.
Identical sequences found, sequence BRK_4 will be ignored.
1 sequences discarded.
Done.
Identical sequences found, sequence BVG_4 will be ignored.
1 sequences discarded.
Done.
Identical sequences found, sequence CCF_6 will be ignored.
Identical sequences found, sequence CCI_4 will be ignored.
Identical sequences found, sequence CCE_6 will be ignored.
Identical sequences found, sequence CCG_4 will be ignored.
4 sequences discarded.
Done.
0 sequences discarded.
Done.
Identical sequences found, sequence AIT_2 will be ignored.
Identical sequences found, sequence YBY will be ignored.
2 sequences discarded.
Done.
Identical sequences found, sequence CQV_4 will be ignored.
Identical sequences found, sequence BIQ_3 will be ignored.
2 sequences discarded.
Done.
Identical sequences found, sequence BMB_1 will be ignored.
Identical sequences found, sequence BMC_1 will be ignored.
Identical sequences found, sequence YCY will be ignored.
3 sequences discarded.
Done.
Identical sequences found, sequence BBS_3 will be ignored.
Identical sequences found, sequence CLC_4 will be ignored.
2 sequences discarded.
Done.
Identical sequences found, sequence YCU will be ignored.
Identical sequences found, sequence AVI_5 will be ignored.
Identical sequences found, sequence ANF_4 will be ignored.
Identical sequences found, sequence YBS will be ignored.
Identical sequences found, sequence ANC_4 will be ignored.
Identical sequences found, sequence ANH_4 will be ignored.
Identical sequences found, sequence ANK_4 will be ignored.
7 sequences discarded.
Done.
Identical sequences found, sequence ANV_5 will be ignored.
Identical sequences found, sequence AVM_5 will be ignored.
Identical sequences found, sequence CQQ_4 will be ignored.
Identical sequences found, sequence CLS_4 will be ignored.
Identical sequences found, sequence CLT_5 will be ignored.
Identical sequences found, sequence CMD_6 will be ignored.
Identical sequences found, sequence CME_6 will be ignored.
Identical sequences found, sequence CQR_4 will be ignored.
Identical sequences found, sequence ANS_2 will be ignored.
Identical sequences found, sequence CMN_8 will be ignored.
Identical sequences found, sequence CML_8 will be ignored.
Identical sequences found, sequence CMM_8 will be ignored.
Identical sequences found, sequence CMB_6 will be ignored.
Identical sequences found, sequence APD_4 will be ignored.
Identical sequences found, sequence AVC_3 will be ignored.
Identical sequences found, sequence CLV_5 will be ignored.
Identical sequences found, sequence APF_4 will be ignored.
Identical sequences found, sequence AVD_3 will be ignored.
Identical sequences found, sequence CLL_4 will be ignored.
Identical sequences found, sequence CLP_4 will be ignored.
20 sequences discarded.
Done.
```

Process the results in R and generate tab file for iSMC:

```r
generateTab <- function(population) {
  d<-read.table(paste0("Output/", population, "/ismc_breakpoints.txt"), sep = ",")
  t<-data.frame(id=1:nrow(d), start=1, stop=d$V2 - d$V1 + 1, v4=0, v5=d$V2 - d$V1, v6=0, v7=d$V2 - d$V1)
  write.table(t, file = paste0("Output/", population, "/ismc.tab"), row.names = F, col.names = F, sep ="\t")
}
for (pop in names(len)) {
  p <- gsub(" ", "", pop)
  generateTab(p)
}
```


Run iSMC analyses
=================

```bash
get_pairs() {
  PAIRS=""
  N=$1
  N=$((N-1))
  for i in $( seq 0 $((N-1)) ); do
    if [ $i = 0 ]; then
      PAIRS="($i,$((i+1))"
    else
      PAIRS="$PAIRS,$i,$((i+1))"
    fi
  done
  PAIRS="$PAIRS,$N,0)"
}

run_ismc_h() {
  cd Output/$1/
  N=`grep '>' ismc_alignment.fasta | wc -l`
  get_pairs $N
  ismc param=../../ismc_opt.bpp POPULATION=$1 diploid_indices=$PAIRS >& ../../Log/${1}_ismc.log
  rm -rf ziphmm_*
  cd ../..
  grep AIC Log/${1}_ismc.log
}

export -f get_pairs
export -f run_ismc_h
```

```r
unlink("runIsmcH.sh")
for (pop in names(len)) {
  p <- gsub(" ", "", pop)
  cat("run_ismc_h ", p, "\n", sep = "",
      file = "runIsmcH.sh",
      append = TRUE)
}
```

Run rhoSMC with 5 rho classes. We set a tolerance of 1e-1 to save time, we do not need many decimals for the estimate of rho!

```bash
run_ismc_nh() {
  mkdir -p Output5rc/$1
  cd Output5rc/$1/
  cp ../../Output/$1/ismc* .
  cp ../../Output/$1/$1_backup_params.txt .
  N=`grep '>' ismc_alignment.fasta | wc -l`
  M=`grep 'prime' ../../Output/$1/${1}_estimates.txt | wc -l`
  get_pairs $N
  ismc --noninteractive=yes param=../../rhosmc_opt.bpp POPULATION=$1 diploid_indices=$PAIRS init_number_knots=$((M-2)) max_number_knots=$((M-2)) >& ../../Log/${1}_5rc.log
  rm -rf ziphmm_*
  cd ../..
}

export -f run_ismc_nh
```

```r
unlink("runIsmcNH.sh")
for (pop in names(len)) {
  p <- gsub(" ", "", pop)
  cat("run_ismc_nh ", p, "\n", sep = "",
      file = "runIsmcNH.sh",
      append = TRUE)
}
```


Analyse diploid populations
===========================

```r
info<-read.csv("../IsolatesInfo.csv")
infoht <- subset(info, Zygosity == "heterozygous" & Ploidy == 2) #170 strains
lst2<-split(infoht$Isolates_IDs, infoht$Population)
len2<-sapply(lst2, length)
len2<-len2[len2>1]
```

this leads to 11 populations (Italian wine being split in two):

```
          African Cocoa       African Palm Wine                Ale Beer 
                     13                       5                       2 
               Alpechin Asian Rice Fermentation              Bioethanol 
                      2                       7                      23 
                  Dairy     Human French Guiana          Italian Wine 1 
                     26                      24                      11 
         Italian Wine 2                  Mezcal                    Sake 
                     13                       7                      36 
```

Create VCF files:

```r
for (pop in names(len2)) {
  p <- gsub(" ", "", pop)
  dir.create(paste0("VCF/", p), recursive = TRUE)
  write(lst2[[pop]], paste0("VCF/", p, "/", p, ".panel"))
}
```

(We use the VCF file from the MSMC directory.)

```r
unlink("runBcftoolsExtract.sh")
for (pop in names(len2)) {
  p <- gsub(" ", "", pop)
  cat(paste0("bcftools view -V indels -S VCF/", p, "/", p, ".panel -o VCF/", p, "/", p, ".vcf.gz -O z ../MSMC/1011Matrix.simplified.gvcf.gz\n"),
      file = "runBcftoolsExtract.sh",
      append = TRUE)
}
```

Then generate the iSMC tab file (no callability mask is considered here):

```r
unlink("runGenerateTabFile.sh")
for (pop in names(len2)) {
  p <- gsub(" ", "", pop)
  dir.create(paste0("Output2/", p), recursive = TRUE)
  cat(paste0("python3 generate_tab.py VCF/", p, "/", p, ".vcf.gz > Output2/", p, "/ismc.tab\n"),
      file = "runGenerateTabFile.sh",
      append = TRUE)
}
```

```bash
mkdir Log2

run_ismc_h2() {
  cd Output2/$1/
  ismc param=../../ismc_opt.bpp POPULATION=$1 input_file_type=VCF sequence_file_path=../../VCF/$1/${1}.vcf.gz seq_compression_type=gzip >& ../../Log2/${1}_ismc.log
  rm -rf ziphmm_*
  cd ../..
  grep AIC Log2/${1}_ismc.log
}

export -f run_ismc_h2
```

```r
unlink("runIsmcH2.sh")
for (pop in names(len2)) {
  p <- gsub(" ", "", pop)
  cat("run_ismc_h2 ", p, "\n", sep = "",
      file = "runIsmcH2.sh",
      append = TRUE)
}
```

Run rhoSMC with 5 rho classes. We set a tolerance of 1e-1 to save time, we do not need many decimals for the estimate of rho!

```bash
run_ismc_nh2() {
  mkdir -p Output5rc2/$1
  cd Output5rc2/$1/
  cp ../../Output2/$1/ismc* .
  cp ../../Output2/$1/$1_backup_params.txt .
  M=`grep 'prime' ../../Output2/$1/${1}_estimates.txt | wc -l`
  ismc --noninteractive=yes param=../../rhosmc_opt.bpp POPULATION=$1 input_file_type=VCF sequence_file_path=../../VCF/$1/${1}.vcf.gz seq_compression_type=gzip init_number_knots=$((M-2)) max_number_knots=$((M-2)) >& ../../Log2/${1}_5rc.log
  rm -rf ziphmm_*
  cd ../..
}

export -f run_ismc_nh2
```

```r
unlink("runIsmcNH2.sh")
for (pop in names(len2)) {
  p <- gsub(" ", "", pop)
  cat("run_ismc_nh2 ", p, "\n", sep = "",
      file = "runIsmcNH2.sh",
      append = TRUE)
}
```

