# Reconstruct demographic histories with MSMC

## Select isolates

We select isolates that are haploid or homozygous polyploid, so that de novo assembled genomes represent "true" haplotypes:

```r
info<-read.csv("../IsolatesInfo.csv")
infoh <- subset(info, Zygosity == "homozygous") #121 strains
lst<-split(infoh$Assembly_IDs, infoh$Population)
len<-sapply(lst, length)
len<-len[len>1]
```

this leads to 10 populations (Italian wine being split in two):
```
"African Palm Wine"  "Alpechin"           "Bioethanol"        
"Ecuadorian"         "Far East Asian"     "Italian Wine 1"    
"Italian Wine 2"     "Malaysian"          "Mediterranean Oak" 
"North American Oak" "Sake"         
```

```r
infoht <- subset(info, Zygosity == "heterozygous" & Ploidy == 2) #170 strains
lst2<-split(infoht$Isolates_IDs, infoht$Population)
len2<-sapply(lst2, length)
```

12 populations:

```
"African Beer"            "African Cocoa"          
"African Palm Wine"       "Ale Beer"               
"Alpechin"                "Asian Rice Fermentation"
"Bioethanol"              "Dairy"                  
"Human French Guiana"     "Italian Wine 1"         
"Italian Wine 2"          "Mezcal"                 
"Sake"                   
```

```r
union(names(len), names(len2)) # We can cover all populations!
intersect(names(len), names(len2)) # For 5 populations we can use both types of data
```

### Generate MSMC input files from genome alignments:

```r
unlink("runMaffilter.sh")
for (pop in names(len)) {
  system(paste("mkdir -p MGA/", gsub(" ", "", pop), sep = ""))
  cat("maffilter param=maffilter-msmcoutput.bpp ISOLATES=\"(", paste(lst[[pop]], collapse=","), ")\" POPULATION=", gsub(" ", "", pop), "\n", sep = "",
      file = "runMaffilter.sh",
      append = TRUE)
}
```
Then run the commands in the `runMaffilter.sh` script.

Split input files into chromosome-specific files and remove mitochondrial data:
```r
unlink("runMsmcSplit.sh")
for (pop in names(len)) {
  p <- gsub(" ", "", pop)
  cat("cd MGA/", p, "; awk '{f=\"", p, ".\" $1 \".msmc\"; print >> f; close(f)}' ", p, ".msmc; rm *Mito*; cd ../..;\n", sep = "",
      file = "runMsmcSplit.sh",
      append = TRUE)
}
```

### Generate MSMC input files from the VCF:

Get the 1011 yeast genome variants:

```bash
wget http://1002genomes.u-strasbg.fr/files/1011Matrix.gvcf.gz
```

We filter SNPs and discard genotype info:
```bash
bcftools annotate -x FORMAT,INFO 1011Matrix.gvcf.gz | bgzip -c > 1011Matrix.simplified.gvcf.gz
``

We then extract the diploid heterozygous individuals for each species.

```{r}
unlink("runBcftoolsExtract.sh")
for (pop in names(len2)) {
  x <- lst2[[pop]]
  p <- gsub(" ", "", pop)
  dir.create(paste0("VCF/", p), recursive = TRUE)
  for (i in x) {
     cat(paste0("bcftools view -V indels -s ", i, " 1011Matrix.simplified.gvcf.gz | bgzip -c > VCF/", p, "/", i, ".vcf.gz\n"),
         file = "runBcftoolsExtract.sh",
         append = TRUE)
  }
}
```

We mask all uncalled genotypes, creating a negative mask:

```{r}
require(plyr)
options(scipen=999) #Avoid printing scientific notation in output files
for (pop in names(len2)) {
  x <- lst2[[pop]]
  p <- gsub(" ", "", pop)
  for (i in x) {
    cat(pop, ":", i, "\n")
    vcf <- read.table(paste0("VCF/", p, "/", i, ".vcf.gz"))
    g <- vcf$V10
    j <- which(g == "./.")
    chr <- vcf[j, 1]
    pos <- vcf[j, 2]
    d <- data.frame(chr = chr, start = pos - 1, end = pos)
    ddply(d, .variable = "chr", .fun = function(d) {
      chr <- unique(d$chr)
      write.table(d, paste0("VCF/", p, "/", i, ".", chr, ".bed"), col.names = FALSE, row.names = FALSE, quote = FALSE)
    })
  }
}
```

We also need a callable mask. As none is provided with the original VCF, we use the same as for the de novo assemblies.
This corresponds to the blocks after filtering and before merging (as merging adds missing data between merged blocks if the are no exactly subsequent).
The information is in the statistics5 files.

```r
require(plyr)
for (pop in names(len2)) {
  p <- gsub(" ", "", pop)
  m <- read.table(paste0("../AlignmentProcessing/", p, "/", p, ".statistics5.csv"), sep = "\t", header = TRUE)
  # Convert to BED:
  m$Start <- m$Start - 1 # BED is [0,x[
  m$Blocksize <- NULL # Remove column
  ddply(m, .variable = "Chr", .fun = function(d) {
    chr <- unique(d$Chr)
    write.table(d, paste0("VCF/", p, "/", p, ".", chr, ".bed"), col.names = FALSE, row.names = FALSE, quote = FALSE)
  })
}
```

Get the python helper scripts:
```bash
#wget https://raw.githubusercontent.com/stschiff/msmc-tools/master/generate_multihetsep.py
```

We further edit the script to force data to be considered as phased. We added a new argument `--as_phased` for doing so.

This script only works on a single chromosome. We first need to split all VCFs per chromosome.
We create indexes:
```{r}
unlink("runTabix.sh")
for (pop in names(len2)) {
  x <- lst2[[pop]]
  p <- gsub(" ", "", pop)
  for (i in x) {
     cat(paste0("tabix VCF/", p, "/", i, ".vcf.gz\n"),
         file = "runTabix.sh",
         append = TRUE)
  }
}
```

Then we split:
```{r}
unlink("runBcftoolsSplit.sh")
for (pop in names(len2)) {
  x <- lst2[[pop]]
  p <- gsub(" ", "", pop)
  for (i in x) {
     cat(paste0("bcftools index -s VCF/", p, "/", i, ".vcf.gz | cut -f 1 | while read C; do bcftools view -g het -O z -o VCF/", p, "/", i, ".${C}.vcf.gz VCF/", p, "/", i, ".vcf.gz \"${C}\" ; done\n"),
         file = "runBcftoolsSplit.sh",
         append = TRUE)
  }
}
```

Now create the MSMC input files, one per chromosome:
```r
unlink("runMultihetsep.sh")
cmd <- "python3 generate_multihetsep.py --as_phased --mask "
chrs <- read.table("chromosomes.txt")$V2
for (pop in names(len2)) {
  x <- lst2[[pop]]
  p <- gsub(" ", "", pop)
  for (j in chrs) {
    mask <- paste0("VCF/", p, "/", p, ".", j, ".bed")
    for (i in x) {
      mask <- paste0(mask, " --negative_mask VCF/", p, "/", i, ".", j, ".bed")
    }
    cmd1 <- paste0(cmd, mask)
    cmd2 <- cmd1
    for (i in x) {
      cmd2 <- paste0(cmd2, " VCF/", p, "/", i, ".", j, ".vcf.gz")
    }
    cmd2 <- paste0(cmd2, " > VCF/", p, "/", p, ".", j, ".msmc\n")
    cat(cmd2, file = "runMultihetsep.sh", append = TRUE)
  }
}
```


## Run MSMC2 on all genomes per lineage

For the haploid genotypes, which are phased by construction, we can use all pairs of haplotypes:

```r
unlink("runMsmc2AllMga.sh")
for (pop in names(len)) {
  p <- gsub(" ", "", pop)
  cat("msmc2 -p 1*5+20*1+1*5 -o MGA/", p, "/", p, ".out MGA/", p, "/", p, ".*.msmc\n", sep = "",
      file = "runMsmc2AllMga.sh",
      append = TRUE)
}
```

For the diploid (unphased) genomes, we can only perform intra-genotype pairs:

```r
unlink("runMsmc2AllVcf.sh")
for (pop in names(len2)) {
  p <- gsub(" ", "", pop)
  n <- len2[pop]
  pairs <- NA
  for (i in 0:(n-1)) {
    if (is.na(pairs)) {
      pairs <- paste0(2*i, "-", 2*i+1)
    } else {
      pairs <- paste(pairs, paste0(2*i, "-", 2*i+1), sep =",")
    }
  }
  cat("msmc2 -p 1*5+20*1+1*5 -I ", pairs, " -o VCF/", p, "/", p, ".out VCF/", p, "/", p, ".*.msmc\n", sep = "",
      file = "runMsmc2AllVcf.sh",
      append = TRUE)
}
```

## Run MSMC2 on subsamples of lineages

### Pairs

We combine all haplotypes in pairs, so that all haplotype is used at least once.

```r
unlink("runMsmc2_2_Mga.sh")
len.pairs <- len[len > 2] 
for (pop in names(len.pairs)) {
  p <- gsub(" ", "", pop)
  n <- len.pairs[pop]
  x <- 0:(n-1)
  if (length(x) %% 2 == 1) { # odd number of haplotypes
    x <- c(x, 0)
  }
  pairs <- c()
  for (i in 1:(length(x)/2)) { 
    pairs <- c(pairs, paste0(x[2*i-1], "-", x[2*i]))
  }
  count <- 1
  for (i in pairs) {
    cat("msmc2 -p 1*5+20*1+1*5 -I ", i, " -o MGA/", p, "/", p, ".2_", count, ".out MGA/", p, "/", p, ".*.msmc\n", sep = "",
        file = "runMsmc2_2_Mga.sh",
        append = TRUE)
    count <- count + 1
  }
}
```

For the diploids, we have a set of genotype pairs:

```r
unlink("runMsmc2_2_Vcf.sh")
for (pop in names(len.pairs)) {
  p <- gsub(" ", "", pop)
  n <- 2*len.pairs[pop]
  x <- 0:(n-1)
  pairs <- c()
  for (i in 1:(length(x)/2)) { 
    pairs <- c(pairs, paste0(x[2*i-1], "-", x[2*i]))
  }
  count <- 1
  for (i in pairs) {
    cat("msmc2 -p 1*5+20*1+1*5 -I ", i, " -o VCF/", p, "/", p, ".2_", count, ".out VCF/", p, "/", p, ".*.msmc\n", sep = "",
        file = "runMsmc2_2_Vcf.sh",
        append = TRUE)
    count <- count + 1
  }
}
```

