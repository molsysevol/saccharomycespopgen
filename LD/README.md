# Compute LD decay

## Data extraction

We select diploid isolates:

```r
info<-read.csv("../IsolatesInfo.csv")

infoh <- subset(info, Zygosity == "homozygous") #121 strains
lst<-split(infoh$Assembly_IDs, infoh$Population)
len<-sapply(lst, length)
len<-len[len>=4] # Need at least 2 "diploids" to compute LD

infoht <- subset(info, Zygosity == "heterozygous" & Ploidy == 2) #170 strains
lst2<-split(infoht$Isolates_IDs, infoht$Population)
len2<-sapply(lst2, length)
len2<-len2[len2>1]
```

For the haploids, convert MAF files to VCF:
```r
unlink("runMaffilter.sh")
for (pop in names(len)) {
  p <- gsub(" ", "", pop)
  dir.create(paste0("MGA/", p), recursive = TRUE)
  n <- floor(length(lst[[pop]])/2)
  iso <- ""
  for (i in 1:n) {
    x <- paste0("(", lst[[pop]][2*i-1], ",", lst[[pop]][2*i], ")")
    if (iso == "") iso <- x
    else iso <- paste0(iso, ",", x)
  }
  cat("maffilter param=maffilter-vcfoutput.bpp ISOLATES=\"(", iso, ")\" POPULATION=", p, "\n", sep = "",
      file = "runMaffilter.sh",
      append = TRUE)
}
```

``bash
mkdir Log
```
Run the `runMaffilter.sh`commands.

Then compress the VCFs:
```r
for (pop in names(len)) {
  p <- gsub(" ", "", pop)
  system(paste0("bgzip MGA/", p, "/", p, ".vcf"))
}
```

For the diploids, extract from the VCF:

```r
for (pop in names(len2)) {
  p <- gsub(" ", "", pop)
  dir.create(paste0("VCF/", p), recursive = TRUE)
  write(lst2[[pop]], paste0("VCF/", p, "/", p, ".panel"))
}
```

(We use the VCF file from the MSMC directory.)
```r
unlink("runBcftoolsExtract.sh")
for (pop in names(len2)) {
  p <- gsub(" ", "", pop)
  cat(paste0("bcftools view -V indels -S VCF/", p, "/", p, ".panel -o VCF/", p, "/", p, ".vcf.gz -O z ../MSMC/1011Matrix.simplified.gvcf.gz\n"),
      file = "runBcftoolsExtract.sh",
      append = TRUE)
}
```

## Convert to PLINK

### First need to rename chromosomes:

```r
unlink("runBcftoolsRenameChrMGA.sh")
for (pop in names(len)) {
  p <- gsub(" ", "", pop)
  cat(paste0("tabix MGA/", p, "/", p, ".vcf.gz; bcftools annotate --rename-chrs chromosomes.txt MGA/", p, "/", p, ".vcf.gz -o MGA/", p, "/", p, ".newchr.vcf.gz -O z\n"),
      file = "runBcftoolsRenameChrMGA.sh",
      append = TRUE)
}
```

```r
unlink("runBcftoolsRenameChrVCF.sh")
for (pop in names(len2)) {
  p <- gsub(" ", "", pop)
  cat(paste0("tabix VCF/", p, "/", p, ".vcf.gz; bcftools annotate --rename-chrs chromosomes.txt VCF/", p, "/", p, ".vcf.gz -o VCF/", p, "/", p, ".newchr.vcf.gz -O z\n"),
      file = "runBcftoolsRenameChrVCF.sh",
      append = TRUE)
}
```

### Then convert:

```r
unlink("runVcf2PlinkMGA.sh")
for (pop in names(len)) {
  p <- gsub(" ", "", pop)
  cat(paste0("vcftools --gzvcf MGA/", p, "/", p, ".newchr.vcf.gz --maf 0.2 --max-missing 1 --plink-tped --out MGA/", p, "/", p, "\n"),
      file = "runVcf2PlinkMGA.sh",
      append = TRUE)
}
```

```r
unlink("runVcf2PlinkVCF.sh")
for (pop in names(len2)) {
  p <- gsub(" ", "", pop)
  cat(paste0("vcftools --gzvcf VCF/", p, "/", p, ".newchr.vcf.gz --maf 0.2 --max-missing 1 --plink-tped --out VCF/", p, "/", p, "\n"),
      file = "runVcf2PlinkVCF.sh",
      append = TRUE)
}
```

## Compute LD

```r
unlink("runPlinkLD_MGA.sh")
for (pop in names(len)) {
  p <- gsub(" ", "", pop)
  cat(paste0("cd  MGA/", p, "/ ; plink1.9 --tfile ", p, " --r2 --ld-window-r2 0 --ld-window-kb 10000; cd ../..\n"),
      file = "runPlinkLD_MGA.sh",
      append = TRUE)
}
```

```r
unlink("runPlinkLD_VCF.sh")
for (pop in names(len2)) {
  p <- gsub(" ", "", pop)
  cat(paste0("cd  VCF/", p, "/ ; plink1.9 --tfile ", p, " --r2 --ld-window-r2 0 --ld-window-kb 10000; cd ../..\n"),
      file = "runPlinkLD_VCF.sh",
      append = TRUE)
}
```

See LDAnalysis.R` for a plotting/analysis of the results.

