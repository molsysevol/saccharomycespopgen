# Compiling gene sets implicated in wild and domesticated environments of _S. cerevisiae_
# Created on 31/12/2021 by Maximilian Raas

### Compiling gene sets

Gene sets are compiled mostly using AmiGO2 Genes and gene products search: http://amigo.geneontology.org/amigo/search/bioentity (last accessed August 2021)
All relevant genes were downloaded using the "Custom DL" option and selecting the "Gene/product (bioentity_label)" and "Synonyms (synonym)" pools to get the systematic names of all genes. Genes were downloaded into a file as GeneSetName/GeneSetNameAll.

#### Domestic carbon

AmiGO2 search terms: glucose" OR "xylose" OR "maltose" OR "fructose" OR "sucrose" -unknown

#### Heat Stress
AmiGO2 search terms: "heat stress" "temperature" -unkown

#### Meiosis
AmiGO2 search terms: "meiosis" "sporulation" -uknown

#### Metal Stress
Compiled from Thorsen _et al._ (2009; https://doi.org/10.1186/1471-2164-10-105) Additional File 1 on genes involved in arsenic response and from Jo _et al._ (2008; https://doi.org/10.1093/toxsci/kfm226) Supplementary data table 5 on genes involved in copper response.

#### Osmotic Stress
AmiGO2 search terms: "osmotic stress" -unkown

#### Oxidative Stress
AmiGO2 search terms; "oxidative stress" -unkown

### Filtering gene sets

Gene sets are filtered to only include genes recovered from all lineages (the "FullOverlapList" in directory "GeneOverlap") and to only include genes found in only 1 of the gene sets. Genes found in multiple sets are combined into the gene set "MultipleSets". All remaining genes are combined in the gene set "Other genes". Script to filter gene sets:

```
./IndependentGeneSets.R
```
