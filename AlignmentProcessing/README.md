# Genome alignment processing & gene extraction

## Create directory system for genome alignment processing and gene extraction:

```bash
./mkdirs.sh
```

## Realign alignments locally:

```bash
maffilter param=maffilter-realign.bpp ISOLATES="(AEN_3, AFL_5, ANL_2, ANM_2, ASC_6, AVN_5, AVP_5, AVQ_5, AVR_6, AVS_5, AVT_6, BEI_7, BEK_7, BEM_7, BEN_7, BHA_2, R64)" POPULATION=AfricanBeer
maffilter param=maffilter-realign.bpp ISOLATES="(ANN_4, CQC_6, CQE_6, CQG_6, CQI_6, CQL_4, CQN_4, CQD_6, CQF_6, CQH_6, CQK_4, CQM_4, CQP_4, R64)" POPULATION=AfricanCocoa
maffilter param=maffilter-realign.bpp ISOLATES="(ADV_4, AKB_2, AKC_4, AKD_2, AKE_3, AKG_3, AKH_3, APQ_4, AVV_8, BAA_6, BAB_8, BAC_8, BAD_8, BAE_6, BAF_6, CFA_4, CPS_4, CPT_4, CQA_6, CQB_6, YAC, R64)" POPULATION=AfricanPalmWine
maffilter param=maffilter-realign.bpp ISOLATES="(AAC_6, AEB_8, AEC_8, AGB_1, AQT_4, ASH_3, ATV_5, BRP_4, BRQ_4, BSI_3, CBR_2, CFC_2, CFD_4, CFG_4, CFH, CFM_4, R64)" POPULATION=AleBeer
maffilter param=maffilter-realign.bpp ISOLATES="(ABM_5, AHM_1, AQA_4, ATQ_7, AHL_3, AQD_4, BRK_4, R64)" POPULATION=Alpechin
maffilter param=maffilter-realign.bpp ISOLATES="(BEF_7, BEH_7, BHP_4, BHQ_4, BHR_4, BHS_1, CGN_5, CGR_5, R64)" POPULATION=AsianRiceFermentation
maffilter param=maffilter-realign.bpp ISOLATES="(AEG_8, AEH_3, AFD_4, AFR_5, AGM_1, AHC_2, APG_4, BVB_6, BVD_5, BVF_5, BVH_1, CNB_4, CNE_4, CNG_4, CNI_4, CNL_4, CNN_1, CNQ_1, CNS_4, CNV_4, BVA_4, BVC_6, BVE_4, BVG_4, CMT_8, CNF_4, CNH_4, CNK_4, CNM_1, CNP_1, CNR_1, CNT_4, R64)" POPULATION=Bioethanol
maffilter param=maffilter-realign.bpp ISOLATES="(AQM_4, ARR_6, ASN_8, BFH_3, BFS_8, BFT_8, BFV_8, BGA_7, BGB_7, BGC_8, BGD_8, BGE_8, BGF_8, BGG_8, BGH_1, BGI_1, BGK_1, BGL_1, BGM_1, BGN_1, BGP_1, BGQ_1, BGR_1, BGS_1, BGT_2, BGV_2, R64)" POPULATION=Dairy
maffilter param=maffilter-realign.bpp ISOLATES="(CCC_3, CCD_4, CCE_6, CCF_6, CCG_4, CCH_4, CCI_4, R64)" POPULATION=Ecuadorian
maffilter param=maffilter-realign.bpp ISOLATES="(BAI_7, BAK_7, CCN_1, CCQ_1, CCR_1, CCS_1, CDL_3, YCR, R64)" POPULATION=FarEastAsian
maffilter param=maffilter-realign.bpp ISOLATES="(BCB_3, BCD_8, BCF_8, BCH_5, BCK_8, BCM_8, BCP_8, BDP_5, BDR_6, BDT_6, BEA_6, BEC_6, BEE_6, BCC_3, BCE_8, BCG_8, BCI_8, BCL_5, BCN_8, BCQ_8, BDQ_6, BDS_6, BDV_6, BEB_6, BED_6, R64)" POPULATION=HumanFrenchGuiana
maffilter param=maffilter-realign.bpp ISOLATES="(AIA_1, AIR_1, APT_7, BHV_1, BIG_1, BIR_2, BKG_3, BKL_1, BNF_4, BPC_4, BPP_4, BQE_4, BQI_4, BQS_4, BRF_4, CRC_2, CRI_2, YBY, AIN_1, AIT_2, ASR_1, BIF_1, BIM_3, BKE_1, BKH_1, BLV_3, BNG_4, BPL_2, BQB_4, BQH_4, BQM_4, BRE_4, BVK_2, CRD_2, CRK_2, YBX, R64)" POPULATION=ItalianWine1
maffilter param=maffilter-realign.bpp ISOLATES="(ADR_4, AIF_1, AIP_2, BIA_1, BIC_1, BIP_3, BIS_2, BKF_3, BKN_2, BNE_2, BPM_4, BQG_4, BQQ_4, BRB_4, BSL_4, BSN_5, CQV_4, CRB_2, AGI_1, AIM_2, BAV_7, BIB_1, BIN_3, BIQ_3, BIT_2, BKM_1, BKP_1, BPB_4, BPT_5, BQP_4, BQT_4, BRC_4, BSM_3, BVM_3, CRA_2, R64)" POPULATION=ItalianWine2
maffilter param=maffilter-realign.bpp ISOLATES="(AKM_4, BMA_2, BMB_1, BMC_1, MAL, YCY, R64)" POPULATION=Malaysian
maffilter param=maffilter-realign.bpp ISOLATES="(BBQ_3, BBS_3, BFP_3, BTG_3, CCL_1, CLB_5, CLC_4, CLD_5, R64)" POPULATION=MediterraneanOak
maffilter param=maffilter-realign.bpp ISOLATES="(CPI_4, CPL_4, CPN_4, CPR_4, CPK_4, CPM_4, CPQ_4, R64)" POPULATION=Mezcal
maffilter param=maffilter-realign.bpp ISOLATES="(ACD_7, AKN_3, ANC_4, AND_4, ANE_4, ANF_4, ANG_4, ANH_4, ANI_4, ANK_4, AVI_5, YBS, YCU, R64)" POPULATION=NorthAmericanOak
maffilter param=maffilter-realign.bpp ISOLATES="(AAD_6, ADQ_4, AFM_5, ANR_2, ANT_2, APA_2, APD_4, APF_4, AVC_3, AVL_6, BTS_3, CLK_4, CLM_4, CLP_4, CLR_4, CLT_5, CMB_6, CMD_6, CMF_6, CMI_6, CML_8, CMN_8, CQR_4, ADN_4, AEF_8, AFN_4, ANS_2, ANV_5, APC_4, APE_4, AQN_7, AVD_3, AVM_5, CLI_4, CLL_4, CLN_4, CLQ_4, CLS_4, CLV_5, CMC_6, CME_6, CMH_6, CMK_6, CMM_8, CQQ_4, R64)" POPULATION=Sake
```

## Filter alignments:

```bash
maffilter param=maffilter-filter.bpp ISOLATES="(AEN_3, AFL_5, ANL_2, ANM_2, ASC_6, AVN_5, AVP_5, AVQ_5, AVR_6, AVS_5, AVT_6, BEI_7, BEK_7, BEM_7, BEN_7, BHA_2, R64)" POPULATION=AfricanBeer
maffilter param=maffilter-filter.bpp ISOLATES="(ANN_4, CQC_6, CQE_6, CQG_6, CQI_6, CQL_4, CQN_4, CQD_6, CQF_6, CQH_6, CQK_4, CQM_4, CQP_4, R64)" POPULATION=AfricanCocoa
maffilter param=maffilter-filter.bpp ISOLATES="(ADV_4, AKB_2, AKC_4, AKD_2, AKE_3, AKG_3, AKH_3, APQ_4, AVV_8, BAA_6, BAB_8, BAC_8, BAD_8, BAE_6, BAF_6, CFA_4, CPS_4, CPT_4, CQA_6, CQB_6, YAC, R64)" POPULATION=AfricanPalmWine
maffilter param=maffilter-filter.bpp ISOLATES="(AAC_6, AEB_8, AEC_8, AGB_1, AQT_4, ASH_3, ATV_5, BRP_4, BRQ_4, BSI_3, CBR_2, CFC_2, CFD_4, CFG_4, CFH, CFM_4, R64)" POPULATION=AleBeer
maffilter param=maffilter-filter.bpp ISOLATES="(ABM_5, AHM_1, AQA_4, ATQ_7, AHL_3, AQD_4, BRK_4, R64)" POPULATION=Alpechin
maffilter param=maffilter-filter.bpp ISOLATES="(BEF_7, BEH_7, BHP_4, BHQ_4, BHR_4, BHS_1, CGN_5, CGR_5, R64)" POPULATION=AsianRiceFermentation
maffilter param=maffilter-filter.bpp ISOLATES="(AEG_8, AEH_3, AFD_4, AFR_5, AGM_1, AHC_2, APG_4, BVB_6, BVD_5, BVF_5, BVH_1, CNB_4, CNE_4, CNG_4, CNI_4, CNL_4, CNN_1, CNQ_1, CNS_4, CNV_4, BVA_4, BVC_6, BVE_4, BVG_4, CMT_8, CNF_4, CNH_4, CNK_4, CNM_1, CNP_1, CNR_1, CNT_4, R64)" POPULATION=Bioethanol
maffilter param=maffilter-filter.bpp ISOLATES="(AQM_4, ARR_6, ASN_8, BFH_3, BFS_8, BFT_8, BFV_8, BGA_7, BGB_7, BGC_8, BGD_8, BGE_8, BGF_8, BGG_8, BGH_1, BGI_1, BGK_1, BGL_1, BGM_1, BGN_1, BGP_1, BGQ_1, BGR_1, BGS_1, BGT_2, BGV_2, R64)" POPULATION=Dairy
maffilter param=maffilter-filter.bpp ISOLATES="(CCC_3, CCD_4, CCE_6, CCF_6, CCG_4, CCH_4, CCI_4, R64)" POPULATION=Ecuadorian
maffilter param=maffilter-filter.bpp ISOLATES="(BAI_7, BAK_7, CCN_1, CCQ_1, CCR_1, CCS_1, CDL_3, YCR, R64)" POPULATION=FarEastAsian
maffilter param=maffilter-filter.bpp ISOLATES="(BCB_3, BCD_8, BCF_8, BCH_5, BCK_8, BCM_8, BCP_8, BDP_5, BDR_6, BDT_6, BEA_6, BEC_6, BEE_6, BCC_3, BCE_8, BCG_8, BCI_8, BCL_5, BCN_8, BCQ_8, BDQ_6, BDS_6, BDV_6, BEB_6, BED_6, R64)" POPULATION=HumanFrenchGuiana
maffilter param=maffilter-filter.bpp ISOLATES="(AIA_1, AIR_1, APT_7, BHV_1, BIG_1, BIR_2, BKG_3, BKL_1, BNF_4, BPC_4, BPP_4, BQE_4, BQI_4, BQS_4, BRF_4, CRC_2, CRI_2, YBY, AIN_1, AIT_2, ASR_1, BIF_1, BIM_3, BKE_1, BKH_1, BLV_3, BNG_4, BPL_2, BQB_4, BQH_4, BQM_4, BRE_4, BVK_2, CRD_2, CRK_2, YBX, R64)" POPULATION=ItalianWine1
maffilter param=maffilter-filter.bpp ISOLATES="(ADR_4, AIF_1, AIP_2, BIA_1, BIC_1, BIP_3, BIS_2, BKF_3, BKN_2, BNE_2, BPM_4, BQG_4, BQQ_4, BRB_4, BSL_4, BSN_5, CQV_4, CRB_2, AGI_1, AIM_2, BAV_7, BIB_1, BIN_3, BIQ_3, BIT_2, BKM_1, BKP_1, BPB_4, BPT_5, BQP_4, BQT_4, BRC_4, BSM_3, BVM_3, CRA_2, R64)" POPULATION=ItalianWine2
maffilter param=maffilter-filter.bpp ISOLATES="(AKM_4, BMA_2, BMB_1, BMC_1, MAL, YCY, R64)" POPULATION=Malaysian
maffilter param=maffilter-filter.bpp ISOLATES="(BBQ_3, BBS_3, BFP_3, BTG_3, CCL_1, CLB_5, CLC_4, CLD_5, R64)" POPULATION=MediterraneanOak
maffilter param=maffilter-filter.bpp ISOLATES="(CPI_4, CPL_4, CPN_4, CPR_4, CPK_4, CPM_4, CPQ_4, R64)" POPULATION=Mezcal
maffilter param=maffilter-filter.bpp ISOLATES="(ACD_7, AKN_3, ANC_4, AND_4, ANE_4, ANF_4, ANG_4, ANH_4, ANI_4, ANK_4, AVI_5, YBS, YCU, R64)" POPULATION=NorthAmericanOak
maffilter param=maffilter-filter.bpp ISOLATES="(AAD_6, ADQ_4, AFM_5, ANR_2, ANT_2, APA_2, APD_4, APF_4, AVC_3, AVL_6, BTS_3, CLK_4, CLM_4, CLP_4, CLR_4, CLT_5, CMB_6, CMD_6, CMF_6, CMI_6, CML_8, CMN_8, CQR_4, ADN_4, AEF_8, AFN_4, ANS_2, ANV_5, APC_4, APE_4, AQN_7, AVD_3, AVM_5, CLI_4, CLL_4, CLN_4, CLQ_4, CLS_4, CLV_5, CMC_6, CME_6, CMH_6, CMK_6, CMM_8, CQQ_4, R64)" POPULATION=Sake
```

## Compute general statistics:

```bash
maffilter param=maffilter-statistics.bpp ISOLATES="(AEN_3, AFL_5, ANL_2, ANM_2, ASC_6, AVN_5, AVP_5, AVQ_5, AVR_6, AVS_5, AVT_6, BEI_7, BEK_7, BEM_7, BEN_7, BHA_2, R64)" POPULATION=AfricanBeer
maffilter param=maffilter-statistics.bpp ISOLATES="(ANN_4, CQC_6, CQE_6, CQG_6, CQI_6, CQL_4, CQN_4, CQD_6, CQF_6, CQH_6, CQK_4, CQM_4, CQP_4, R64)" POPULATION=AfricanCocoa
maffilter param=maffilter-statistics.bpp ISOLATES="(ADV_4, AKB_2, AKC_4, AKD_2, AKE_3, AKG_3, AKH_3, APQ_4, AVV_8, BAA_6, BAB_8, BAC_8, BAD_8, BAE_6, BAF_6, CFA_4, CPS_4, CPT_4, CQA_6, CQB_6, YAC, R64)" POPULATION=AfricanPalmWine
maffilter param=maffilter-statistics.bpp ISOLATES="(AAC_6, AEB_8, AEC_8, AGB_1, AQT_4, ASH_3, ATV_5, BRP_4, BRQ_4, BSI_3, CBR_2, CFC_2, CFD_4, CFG_4, CFH, CFM_4, R64)" POPULATION=AleBeer
maffilter param=maffilter-statistics.bpp ISOLATES="(ABM_5, AHM_1, AQA_4, ATQ_7, AHL_3, AQD_4, BRK_4, R64)" POPULATION=Alpechin
maffilter param=maffilter-statistics.bpp ISOLATES="(BEF_7, BEH_7, BHP_4, BHQ_4, BHR_4, BHS_1, CGN_5, CGR_5, R64)" POPULATION=AsianRiceFermentation
maffilter param=maffilter-statistics.bpp ISOLATES="(AEG_8, AEH_3, AFD_4, AFR_5, AGM_1, AHC_2, APG_4, BVB_6, BVD_5, BVF_5, BVH_1, CNB_4, CNE_4, CNG_4, CNI_4, CNL_4, CNN_1, CNQ_1, CNS_4, CNV_4, BVA_4, BVC_6, BVE_4, BVG_4, CMT_8, CNF_4, CNH_4, CNK_4, CNM_1, CNP_1, CNR_1, CNT_4, R64)" POPULATION=Bioethanol
maffilter param=maffilter-statistics.bpp ISOLATES="(AQM_4, ARR_6, ASN_8, BFH_3, BFS_8, BFT_8, BFV_8, BGA_7, BGB_7, BGC_8, BGD_8, BGE_8, BGF_8, BGG_8, BGH_1, BGI_1, BGK_1, BGL_1, BGM_1, BGN_1, BGP_1, BGQ_1, BGR_1, BGS_1, BGT_2, BGV_2, R64)" POPULATION=Dairy
maffilter param=maffilter-statistics.bpp ISOLATES="(CCC_3, CCD_4, CCE_6, CCF_6, CCG_4, CCH_4, CCI_4, R64)" POPULATION=Ecuadorian
maffilter param=maffilter-statistics.bpp ISOLATES="(BAI_7, BAK_7, CCN_1, CCQ_1, CCR_1, CCS_1, CDL_3, YCR, R64)" POPULATION=FarEastAsian
maffilter param=maffilter-statistics.bpp ISOLATES="(BCB_3, BCD_8, BCF_8, BCH_5, BCK_8, BCM_8, BCP_8, BDP_5, BDR_6, BDT_6, BEA_6, BEC_6, BEE_6, BCC_3, BCE_8, BCG_8, BCI_8, BCL_5, BCN_8, BCQ_8, BDQ_6, BDS_6, BDV_6, BEB_6, BED_6, R64)" POPULATION=HumanFrenchGuiana
maffilter param=maffilter-statistics.bpp ISOLATES="(AIA_1, AIR_1, APT_7, BHV_1, BIG_1, BIR_2, BKG_3, BKL_1, BNF_4, BPC_4, BPP_4, BQE_4, BQI_4, BQS_4, BRF_4, CRC_2, CRI_2, YBY, AIN_1, AIT_2, ASR_1, BIF_1, BIM_3, BKE_1, BKH_1, BLV_3, BNG_4, BPL_2, BQB_4, BQH_4, BQM_4, BRE_4, BVK_2, CRD_2, CRK_2, YBX, R64)" POPULATION=ItalianWine1
maffilter param=maffilter-statistics.bpp ISOLATES="(ADR_4, AIF_1, AIP_2, BIA_1, BIC_1, BIP_3, BIS_2, BKF_3, BKN_2, BNE_2, BPM_4, BQG_4, BQQ_4, BRB_4, BSL_4, BSN_5, CQV_4, CRB_2, AGI_1, AIM_2, BAV_7, BIB_1, BIN_3, BIQ_3, BIT_2, BKM_1, BKP_1, BPB_4, BPT_5, BQP_4, BQT_4, BRC_4, BSM_3, BVM_3, CRA_2, R64)" POPULATION=ItalianWine2
maffilter param=maffilter-statistics.bpp ISOLATES="(AKM_4, BMA_2, BMB_1, BMC_1, MAL, YCY, R64)" POPULATION=Malaysian
maffilter param=maffilter-statistics.bpp ISOLATES="(BBQ_3, BBS_3, BFP_3, BTG_3, CCL_1, CLB_5, CLC_4, CLD_5, R64)" POPULATION=MediterraneanOak
maffilter param=maffilter-statistics.bpp ISOLATES="(CPI_4, CPL_4, CPN_4, CPR_4, CPK_4, CPM_4, CPQ_4, R64)" POPULATION=Mezcal
maffilter param=maffilter-statistics.bpp ISOLATES="(ACD_7, AKN_3, ANC_4, AND_4, ANE_4, ANF_4, ANG_4, ANH_4, ANI_4, ANK_4, AVI_5, YBS, YCU, R64)" POPULATION=NorthAmericanOak
maffilter param=maffilter-statistics.bpp ISOLATES="(AAD_6, ADQ_4, AFM_5, ANR_2, ANT_2, APA_2, APD_4, APF_4, AVC_3, AVL_6, BTS_3, CLK_4, CLM_4, CLP_4, CLR_4, CLT_5, CMB_6, CMD_6, CMF_6, CMI_6, CML_8, CMN_8, CQR_4, ADN_4, AEF_8, AFN_4, ANS_2, ANV_5, APC_4, APE_4, AQN_7, AVD_3, AVM_5, CLI_4, CLL_4, CLN_4, CLQ_4, CLS_4, CLV_5, CMC_6, CME_6, CMH_6, CMK_6, CMM_8, CQQ_4, R64)" POPULATION=Sake
```

## Extract CDSs from alignments:

```bash
maffilter param=maffilter-extract.bpp ISOLATES="(AEN_3, AFL_5, ANL_2, ANM_2, ASC_6, AVN_5, AVP_5, AVQ_5, AVR_6, AVS_5, AVT_6, BEI_7, BEK_7, BEM_7, BEN_7, BHA_2, R64)" POPULATION=AfricanBeer
maffilter param=maffilter-extract.bpp ISOLATES="(ANN_4, CQC_6, CQE_6, CQG_6, CQI_6, CQL_4, CQN_4, CQD_6, CQF_6, CQH_6, CQK_4, CQM_4, CQP_4, R64)" POPULATION=AfricanCocoa
maffilter param=maffilter-extract.bpp ISOLATES="(ADV_4, AKB_2, AKC_4, AKD_2, AKE_3, AKG_3, AKH_3, APQ_4, AVV_8, BAA_6, BAB_8, BAC_8, BAD_8, BAE_6, BAF_6, CFA_4, CPS_4, CPT_4, CQA_6, CQB_6, YAC, R64)" POPULATION=AfricanPalmWine
maffilter param=maffilter-extract.bpp ISOLATES="(AAC_6, AEB_8, AEC_8, AGB_1, AQT_4, ASH_3, ATV_5, BRP_4, BRQ_4, BSI_3, CBR_2, CFC_2, CFD_4, CFG_4, CFH, CFM_4, R64)" POPULATION=AleBeer
maffilter param=maffilter-extract.bpp ISOLATES="(ABM_5, AHM_1, AQA_4, ATQ_7, AHL_3, AQD_4, BRK_4, R64)" POPULATION=Alpechin
maffilter param=maffilter-extract.bpp ISOLATES="(BEF_7, BEH_7, BHP_4, BHQ_4, BHR_4, BHS_1, CGN_5, CGR_5, R64)" POPULATION=AsianRiceFermentation
maffilter param=maffilter-extract.bpp ISOLATES="(AEG_8, AEH_3, AFD_4, AFR_5, AGM_1, AHC_2, APG_4, BVB_6, BVD_5, BVF_5, BVH_1, CNB_4, CNE_4, CNG_4, CNI_4, CNL_4, CNN_1, CNQ_1, CNS_4, CNV_4, BVA_4, BVC_6, BVE_4, BVG_4, CMT_8, CNF_4, CNH_4, CNK_4, CNM_1, CNP_1, CNR_1, CNT_4, R64)" POPULATION=Bioethanol
maffilter param=maffilter-extract.bpp ISOLATES="(AQM_4, ARR_6, ASN_8, BFH_3, BFS_8, BFT_8, BFV_8, BGA_7, BGB_7, BGC_8, BGD_8, BGE_8, BGF_8, BGG_8, BGH_1, BGI_1, BGK_1, BGL_1, BGM_1, BGN_1, BGP_1, BGQ_1, BGR_1, BGS_1, BGT_2, BGV_2, R64)" POPULATION=Dairy
maffilter param=maffilter-extract.bpp ISOLATES="(CCC_3, CCD_4, CCE_6, CCF_6, CCG_4, CCH_4, CCI_4, R64)" POPULATION=Ecuadorian
maffilter param=maffilter-extract.bpp ISOLATES="(BAI_7, BAK_7, CCN_1, CCQ_1, CCR_1, CCS_1, CDL_3, YCR, R64)" POPULATION=FarEastAsian
maffilter param=maffilter-extract.bpp ISOLATES="(BCB_3, BCD_8, BCF_8, BCH_5, BCK_8, BCM_8, BCP_8, BDP_5, BDR_6, BDT_6, BEA_6, BEC_6, BEE_6, BCC_3, BCE_8, BCG_8, BCI_8, BCL_5, BCN_8, BCQ_8, BDQ_6, BDS_6, BDV_6, BEB_6, BED_6, R64)" POPULATION=HumanFrenchGuiana
maffilter param=maffilter-extract.bpp ISOLATES="(AIA_1, AIR_1, APT_7, BHV_1, BIG_1, BIR_2, BKG_3, BKL_1, BNF_4, BPC_4, BPP_4, BQE_4, BQI_4, BQS_4, BRF_4, CRC_2, CRI_2, YBY, AIN_1, AIT_2, ASR_1, BIF_1, BIM_3, BKE_1, BKH_1, BLV_3, BNG_4, BPL_2, BQB_4, BQH_4, BQM_4, BRE_4, BVK_2, CRD_2, CRK_2, YBX, R64)" POPULATION=ItalianWine1
maffilter param=maffilter-extract.bpp ISOLATES="(ADR_4, AIF_1, AIP_2, BIA_1, BIC_1, BIP_3, BIS_2, BKF_3, BKN_2, BNE_2, BPM_4, BQG_4, BQQ_4, BRB_4, BSL_4, BSN_5, CQV_4, CRB_2, AGI_1, AIM_2, BAV_7, BIB_1, BIN_3, BIQ_3, BIT_2, BKM_1, BKP_1, BPB_4, BPT_5, BQP_4, BQT_4, BRC_4, BSM_3, BVM_3, CRA_2, R64)" POPULATION=ItalianWine2
maffilter param=maffilter-extract.bpp ISOLATES="(AKM_4, BMA_2, BMB_1, BMC_1, MAL, YCY, R64)" POPULATION=Malaysian
maffilter param=maffilter-extract.bpp ISOLATES="(BBQ_3, BBS_3, BFP_3, BTG_3, CCL_1, CLB_5, CLC_4, CLD_5, R64)" POPULATION=MediterraneanOak
maffilter param=maffilter-extract.bpp ISOLATES="(CPI_4, CPL_4, CPN_4, CPR_4, CPK_4, CPM_4, CPQ_4, R64)" POPULATION=Mezcal
maffilter param=maffilter-extract.bpp ISOLATES="(ACD_7, AKN_3, ANC_4, AND_4, ANE_4, ANF_4, ANG_4, ANH_4, ANI_4, ANK_4, AVI_5, YBS, YCU, R64)" POPULATION=NorthAmericanOak
maffilter param=maffilter-extract.bpp ISOLATES="(AAD_6, ADQ_4, AFM_5, ANR_2, ANT_2, APA_2, APD_4, APF_4, AVC_3, AVL_6, BTS_3, CLK_4, CLM_4, CLP_4, CLR_4, CLT_5, CMB_6, CMD_6, CMF_6, CMI_6, CML_8, CMN_8, CQR_4, ADN_4, AEF_8, AFN_4, ANS_2, ANV_5, APC_4, APE_4, AQN_7, AVD_3, AVM_5, CLI_4, CLL_4, CLN_4, CLQ_4, CLS_4, CLV_5, CMC_6, CME_6, CMH_6, CMK_6, CMM_8, CQQ_4, R64)" POPULATION=Sake
```

## Concatenate CDSs into genes:

```bash
Rscript GeneReconstruction.R --args AfricanBeer
Rscript GeneReconstruction.R --args AfricanCocoa
Rscript GeneReconstruction.R --args AfricanPalmWine
Rscript GeneReconstruction.R --args AleBeer
Rscript GeneReconstruction.R --args Alpechin
Rscript GeneReconstruction.R --args AsianRiceFermentation
Rscript GeneReconstruction.R --args Bioethanol
Rscript GeneReconstruction.R --args Dairy
Rscript GeneReconstruction.R --args Ecuadorian
Rscript GeneReconstruction.R --args Far East Asian
Rscript GeneReconstruction.R --args HumanFrenchGuiana
Rscript GeneReconstruction.R --args ItalianWine1
Rscript GeneReconstruction.R --args ItalianWine2
Rscript GeneReconstruction.R --args Malaysian
Rscript GeneReconstruction.R --args MediterraneanOak
Rscript GeneReconstruction.R --args Mezcal
Rscript GeneReconstruction.R --args NorthAmericanOak
Rscript GeneReconstruction.R --args Sake
```

## Check for aberrant stop codons in reconstructed genes:

```bash
python3 FindStop.py -p AfricanBeer
python3 FindStop.py -p AfricanCocoa
python3 FindStop.py -p AfricanPalmWine
python3 FindStop.py -p AleBeer
python3 FindStop.py -p Alpechin
python3 FindStop.py -p AsianRiceFermentation
python3 FindStop.py -p Bioethanol
python3 FindStop.py -p Dairy
python3 FindStop.py -p Ecuadorian
python3 FindStop.py -p FarEastAsian
python3 FindStop.py -p HumanFrenchGuiana
python3 FindStop.py -p ItalianWine1
python3 FindStop.py -p ItalianWine2
python3 FindStop.py -p Malaysian
python3 FindStop.py -p MediterraneanOak
python3 FindStop.py -p Mezcal
python3 FindStop.py -p NorthAmericanOak
python3 FindStop.py -p Sake
```

