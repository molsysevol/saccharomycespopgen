from Bio import SeqIO
import glob
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--population", type=str)
population = parser.parse_args()
population = str(vars(population))
population = population.split("'")[3]

genefiles = list(glob.glob("./{}/Genes/*.fasta").format(population))

GenesWithGaps = list()
GenesWithOneStop = list()
GenesWithMultipleStops = list()

for gfile in genefiles:
	parsed_seq = SeqIO.parse(gfile, "fasta")
	gname = gfile.split("/")[-1].split(".")[0]
	for seq_record in parsed_seq:
		if seq_record.id == "R64":
			coding_DNA = seq_record.seq
			gapcount = coding_DNA.count("-")
			if gapcount > 0:
				print("Gene " + gname + " contains gaps")
				GenesWithGaps.append(gname)
			else:
				protseq = coding_DNA.translate()
				stopcount = protseq.count("*")
				if stopcount > 1:
					print("There are multiple stop codons in " + gname)
					GenesWithMultipleStops.append(gname)
				else:
					print("All is clear for " + gname + "!")
					GenesWithOneStop.append(gname)

print("Amount of genes with only 1 stop is: " + str(len(GenesWithOneStop)))
print("Amount of genes with > 1 stop is: " + str(len(GenesWithMultipleStops)))
print("Amount of genes with gaps is: " + str(len(GenesWithGaps)))
