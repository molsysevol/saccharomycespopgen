#! /usr/bin/python3

from Bio import AlignIO

aln = AlignIO.read('Alignments/Blocks2/chrI.fasta', 'fasta')
for chr in ['II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII', 'XIII', 'XIV', 'XV', 'XVI']:
  print(aln.get_alignment_length())
  aln = aln + AlignIO.read('Alignments/Blocks2/chr%s.fasta' % (chr), 'fasta')

AlignIO.write(aln, 'Alignments.fasta', 'fasta')
