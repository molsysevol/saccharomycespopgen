<!-- Created by jdutheil on 17.05.22 -->

Get multi-species alignments from UCSC:

```bash
mkdir -p Alignments/Origin
cd Alignments/Origin
for chr in I II III IV V VI VII VIII IX X XI XII XIII XIV XV XVI M
do
  echo "Downloading chr$chr..."
  wget https://hgdownload.soe.ucsc.edu/goldenPath/sacCer3/multiz7way/maf/chr$chr.maf.gz
done
cd ../..
```
Note: the mitochondrial chromosome will not be analyzed here.

Project alignment on S. cerevisiae:

```bash
mkdir -p Alignments/Projected
for chr in I II III IV V VI VII VIII IX X XI XII XIII XIV XV XVI 
do
  echo "Projecting chr$chr..."
  echo " * Uncompress source"
  gunzip  Alignments/Origin/chr$chr.maf.gz
  echo " * Project on sacCer"
  maf_project Alignments/Origin/chr$chr.maf sacCer3 > Alignments/Projected/chr${chr}_sacCer3.maf
  echo " * Recompress source"
  gzip  Alignments/Origin/chr$chr.maf
  echo " * Compress projection"
  gzip  Alignments/Projected/chr${chr}_sacCer3.maf
done
```

Extract S. cerevisiae and S. paradoxus sequences:

```bash
mkdir -p Alignments/Pairwise
for chr in I II III IV V VI VII VIII IX X XI XII XIII XIV XV XVI
do
  echo "Extracting species for chr$chr..."
  maffilter param=MafFilter-Extract.bpp CHR=$chr
done
```
The alignments can be found in the 'Alignments/Pairwise' subdirectory.

We then merge according to S. cerevisiae, and output alignment blocks:
```bash
mkdir -p Alignments/Blocks
for chr in I II III IV V VI VII VIII IX X XI XII XIII XIV XV XVI
do
  echo "Extracting species for chr$chr..."
  maffilter param=MafFilter-Convert.bpp CHR=$chr
done
```

We then create a file list:
```r
library(data.table)
library(plyr)
dat.lst <- list()
for (chr in c("I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII", "XIV", "XV", "XVI")) {
  dat.lst[[chr]] <- read.table(paste0("Alignments/Blocks/chr", chr, ".blocks-statistics.csv"), header = TRUE)
}
stats <- rbindlist(dat.lst)
stats <- ddply(stats, .variables = "Chr", .fun = function(d) { d$Index <- 1:nrow(d); return(d) })

lst <- paste0("Alignments/Blocks/", stats$Chr, "_", stats$Index, "-", stats$Chr, "-", stats$Start, "-", stats$Stop, ".fasta")
write(lst, file = "Alignments.lst")
```

Finally, we can run CoalHMM:
```r
coalhmm param=CoalHMM-I.bpp
```

We compare with an IM model. IMCoalHMM cannot reset the HMM, so we merge all blocks per chromosome and fill with 'N's:
```bash
mkdir -p Alignments/Blocks2
for chr in I II III IV V VI VII VIII IX X XI XII XIII XIV XV XVI
do
  echo "Extracting species for chr$chr..."
  maffilter param=MafFilter-Convert2.bpp CHR=$chr
done
```

We then concatenate all chromosome into a single alignment:
```bash
python3 mergeFasta.py
```

We then run IM-CoalHMM:
```bash
mkdir -p IMCoalHMM/
docker run -it --rm -v $PWD:/mnt -w /mnt imcoalhmm prepare-alignments.py Alignments.fasta fasta IMCoalHMM/AllChr
docker run -it --rm -v $PWD:/mnt -w /mnt imcoalhmm initial-migration-model.py IMCoalHMM/AllChr
```

