from Bio import SearchIO
from Bio import SeqIO
import glob
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--population", type=str)
population = parser.parse_args()
population = str(vars(population))
population = population.split("'")[3]


cds = open("./SparadoxusCDS.fasta", "r")
parsed_cds = SeqIO.to_dict(SeqIO.parse(cds, "fasta"))

print(parsed_cds.keys())

blastoutput = list(glob.glob("./{}/BlastOutFiles/*.xml".format(population)))

matchingids = list()

for i, blast_file in enumerate(blastoutput):
    blast_result = SearchIO.read(blast_file, "blast-xml")
    if len(blast_result) == 0:
        pass
    else:
        first_blast_result = blast_result[0]
        Geneid = first_blast_result.query_id
        Ingroupfile = "../AlignmentProcessing/{}/Genes/".format(population) + Geneid + ".fasta"
        if (os.path.isfile(Ingroupfile)):
            result_id = "lcl|" + first_blast_result.id
            if (result_id in parsed_cds.keys()):
                CDSaln = list(SeqIO.parse(Ingroupfile, "fasta"))
                result = parsed_cds[result_id]
                result.id = "S_paradoxus"
                result.description = ""
                result.name = ""
                CDSaln.append(result)
                outfile = "./{}/BlastOutSeqs/Unaligned/".format(popname) + Geneid + ".fasta"
                SeqIO.write(CDSaln, outfile, "fasta")
                matchingids.append(Geneid)
            else:
                print("No outgroup file found for entry %s. Id was %s\n" % (Geneid, first_blast_result.id))
        else:
            print("No ingroup file found for entry %ss. CDS file was %s\n" % (Geneid, Ingroupfile))
