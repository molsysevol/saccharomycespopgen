import glob
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--population", type=str)
population = parser.parse_args()
population = str(vars(population))
population = population.split("'")[3]

unalignedfiles = list(glob.glob("./{}/BlastOutSeqs/Unaligned/*.fasta".format(population)))
commandslist = list()

for i in unalignedfiles:
    gene = i.split(".")[1].split("/")[-1]
    command = "java -jar ./macse_v2.05.jar -prog alignSequences -seq {}/BlastOutSeqs/Unaligned/".format(population) + gene + ".fasta -out_NT {}/BlastOutSeqs/Aligned/".format(population) + gene + ".fasta -out_AA {}/BlastOutSeqs/Aligned_AA/".format(population) + gene + "_AA.fasta"
    commandslist.append(command)

with open('MACSEcommands{}.sh'.format(population), 'w') as f:
    for item in commandslist:
        f.write("%s\n" % item)
