# Outgroup matching & realigning

# Last updated by Maximilian Raas on 17/12/2021

#### Create directory system:
```
./mkdirs.sh
```

#### Create local _S. paradoxus_ blast reference database:
Download the _S. paradoxus_ CDS file from https://ftp.ncbi.nlm.nih.gov/genomes/refseq/fungi/Saccharomyces_paradoxus/latest_assembly_versions/GCF_002079055.1_ASM207905v1/

Compile local blast database from S. paradoxus CDS:
```
makeblastdb -in SparadoxusCDS.fasta -parse_seqids -blastdb_version 5 -title "S. paradoxus" -dbtype nucl
```

#### Run Blastn on degapped R64 sequences isolated from genome alignments:
Afrian Beer
```
python3 BlastOutGroup.py -p AfricanBeer
```

African Cocoa
```
python3 BlastOutGroup.py -p AfricanCocoa
```

African Palm Wine
```
python3 BlastOutGroup.py -p AfricanPalmWine
```

Ale Beer
```
python3 BlastOutGroup.py -p AleBeer
```

Alpechin
```
python3 BlastOutGroup.py -p Alpechin
```

Asian Rice Fermentation
```
python3 BlastOutGroup.py -p AsianRiceFermentation
```

Bioethanol
```
python3 BlastOutGroup.py -p Bioethanol
```

Dairy
```
python3 BlastOutGroup.py -p Dairy
```

Ecuadorian
```
python3 BlastOutGroup.py -p Ecuadorian
```

Far East Asian
```
python3 BlastOutGroup.py -p FarEastAsian
```

Human French Guiana
```
python3 BlastOutGroup.py -p HumanFrenchGuiana
```

Italian Wine 1
```
python3 BlastOutGroup.py -p ItalianWine1
```

Italian Wine 2
```
python3 BlastOutGroup.py -p ItalianWine2
```

Malaysian
```
python3 BlastOutGroup.py -p Malaysian
```

Mediterranean Oak
```
python3 BlastOutGroup.py -p MediterraneanOak
```

Mezcal
```
python3 BlastOutGroup.py -p Mezcal
```

North American Oak
```
python3 BlastOutGroup.py -p NorthAmericanOak
```

Sake
```
python3 BlastOutGroup.py -p Sake
```

#### Install MACSE2.0:

Download executable MACSE jar file from https://bioweb.supagro.inra.fr/macse/index.php?menu=usage (last accessed January 2021)

#### Create MACSE commands:

African Beer
```
python3 MACSEcommands.py -p AfricanBeer
```

African Cococa
```
python3 MACSEcommands.py -p AfricanCocoa
```

African Palm Wine
```
python3 MACSEcommands.py -p AfricanPalmWine
```

Ale Beer
```
python3 MACSEcommands.py -p AleBeer
```

Alpechin
```
python3 MACSEcommands.py -p Alpechin
```

Asian Rice Fermentation
```
python3 MACSEcommands.py -p AsianRiceFermentation
```

Bioethanol
```
python3 MACSEcommands.py -p Bioethanol
```

Dairy
```
python3 MACSEcommands.py -p Dairy
```

Ecuadorian
```
python3 MACSEcommands.py -p Ecuadorian
```

Far East Asian
```
python3 MACSEcommands.py -p FarEastAsian
```

Human French Guiana
```
python3 MACSEcommands.py -p HumanFrenchGuiana
```

Italian Wine 1
```
python3 MACSEcommands.py -p ItalianWine1
```

Italian Wine 2
```
python3 MACSEcommands.py -p ItalianWine2
```

Malaysian
```
python3 MACSEcommands.py -p Malaysian
```

Mediterranean Oak
```
python3 MACSEcommands.py -p MediterraneanOak
```

Mezcal
```
python3 MACSEcommands.py -p Mezcal
```

North American Oak
```
python3 MACSEcommands.py -p NorthAmericanOak
```

Sake
```
python3 MACSEcommands.py -p Sake
```

#### Execute MACSE commands:

African Beer
```
./MACSEcommandsAfricanBeer.sh
```

African Cocoa
```
./MACSEcommandsAfricanCocoa.sh
```

African Palm Wine
```
./MACSEcommandsAfricanPalmWine.sh
```

Ale Beer
```
./MACSEcommandsAleBeer.sh
```

Alpechin
```
./MACSEcommandsAlpechin.sh
```

Asian Rice Fermentation
```
./MACSEcommandsAsianRiceFermentation.sh
```

Bioethanol
```
./MACSEcommandsBioethanol.sh
```

Dairy
```
./MACSEcommandsDairy.sh
```

Ecuadorian
```
./MACSEcommandsEcuadorian.sh
```

Far East Asian
```
./MACSEcommandsFarEastAsian.sh
```

Human French Guiana
```
./MACSEcommandsHumanFrenchGuiana.sh
```

Italian Wine 1
```
./MACSEcommandsItalianWine1.sh
```

Italian Wine 2
```
./MACSEcommandsItalianWine2.sh
```

Malaysian
```
./MACSEcommandsMalaysian.sh
```

Mediterranean Oak
```
./MACSEcommandsMediterraneanOak.sh
```

Mezcal
```
./MACSEcommandsMezcal.sh
```

North American Oak
```
./MACSEcommandsNorthAmericanOak.sh
```

Sake
```
./MACSEcommandsSake.sh
```

#### MACSE output, both nucleotide (NT) and amino acid (AA) alignment files, is stored as gzipped tar archives. To access these alignment files use:

NT:
```
extract_NT() {
  cd $1/BlastOutSeqs/Aligned_NT/
  tar xvzf $1.Aligned_NT.fasta.tar.gz
  cd ../../..
}
extract_NT AfricanBeer
extract_NT AfricanCocoa
extract_NT AfricanPalmWine
extract_NT AleBeer
extract_NT Alpechin
extract_NT AsianRiceFermentation
extract_NT Bioethanol
extract_NT Dairy
extract_NT Ecuadorian
extract_NT FarEastAsian
extract_NT HumanFrenchGuiana
extract_NT ItalianWine1
extract_NT ItalianWine2
extract_NT Malaysian
extract_NT MediterraneanOak
extract_NT Mezcal
extract_NT NorthAmericanOak
extract_NT Sake
```

AA:
```
extract_AA() {
  cd $1/BlastOutSeqs/Aligned_AA/
  tar xvzf $1.Aligned_AA.fasta.tar.gz
  cd ../../..
}
extract_AA AfricanBeer
extract_AA AfricanCocoa
extract_AA AfricanPalmWine
extract_AA AleBeer
extract_AA Alpechin
extract_AA AsianRiceFermentation
extract_AA Bioethanol
extract_AA Dairy
extract_AA Ecuadorian
extract_AA FarEastAsian
extract_AA HumanFrenchGuiana
extract_AA ItalianWine1
extract_AA ItalianWine2
extract_AA Malaysian
extract_AA MediterraneanOak
extract_AA Mezcal
extract_AA NorthAmericanOak
extract_AA Sake
```
