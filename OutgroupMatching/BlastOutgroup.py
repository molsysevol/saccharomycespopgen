from Bio import SeqIO
from Bio.Blast.Applications import NcbiblastnCommandline
import glob

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--population", type=str)
population = parser.parse_args()
population = str(vars(population))
population = population.split("'")[3]

genes = list(glob.glob("../AlignmentProcessing/{}/Genes/*.fasta".format(population)))

for gene in genes:
	gname = gene.split("/")[-1].split(".")[0]
	parsed_seq = SeqIO.parse(gene, "fasta")
	for seq_record in parsed_seq:
		if seq_record.id == "R64":
			seq_record.seq = seq_record.seq.ungap("-")
			outfile = "./{}/R64ungapped/".format(popname) + gname + ".fasta"
			SeqIO.write(seq_record, outfile, "fasta")

for i, gene in enumerate(genes):
    print("%i\t%s\n" % (i, gene))
    g = gene.split("/")[-1].split(".")[0]
    q = "./{}/R64ungapped/".format(popname) + g + ".fasta"
    o = "./{}/BlastOutFiles/".format(popname) + g + ".xml"                    
    blastn_cline = NcbiblastnCommandline(query = q,
                                         db = "SparadoxusCDS", evalue = 0.001, outfmt = 5,
                                         out = o)
    print(blastn_cline)
    blastn_cline
    stdout, stderr = blastn_cline()
