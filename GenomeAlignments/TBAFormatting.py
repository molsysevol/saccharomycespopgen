import argparse
from Bio import SeqIO
import glob

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--population", type=str)
population = parser.parse_args()
population = str(vars(population))
population = population.split("'")[3]

seq_files = list(glob.glob("{}/*.re.fa".format(population)))

for seqfile in seq_files:
	sequences_list = list()
	parsed_sequences = SeqIO.parse(seqfile, "fasta")
	isolate = seqfile.split("/")[-1].split(".")[0]
	for seq_record in parsed_sequences:
		seq_record.id = isolate + ":" + str(seq_record.name) + ":1:+:" + str(len(seq_record.seq))
		sequences_list.append(seq_record)
		seq_record.description = ""
		seq_record.name = ""
	outfile = "{}/".format(population) + isolate
	SeqIO.write(sequences_list, outfile, "fasta")
