# Genome alignments using TBA

Created on 13/12/2021 by Maximilian Raas, last modified 04/06/2022 by Maximilian Raas

#### Retrieving genome assemblies

Download genome assemblies
```
wget http://1002genomes.u-strasbg.fr/files/1011Assemblies.tar.gz
```

Decompress files
```
tar xvf 1011Assemblies.tar.gz
```

Create directory structure
```
./mkdirs.sh
```

#### Format genomes to make them compatible with the format required for TBA

African Beer
```
python3 TBAFormatting.py -p AfricanBeer
```

African Cocoa
```
python3 TBAFormatting.py -p AfricanCocoa
```

African Palm Wine
```
python3 TBAFormatting.py -p AfricanPalmWine
```

Ale Beer
```
python3 TBAFormatting.py -p AleBeer
```

Alpechin
```
python3 TBAFormatting.py -p Alpechin
```

Asian Rice Fermentation
```
python3 TBAFormatting.py -p AsianRiceFermentation
```

Bioethanol
```
python3 TBAFormatting.py -p Bioethanol
```

Dairy
```
python3 TBAFormatting.py -p Dairy
```

Ecuadorian
```
python3 TBAFormatting.py -p Ecuadorian
```

Far East Asian
```
python3 TBAFormatting.py -p FarEastAsian
```

Human French Guiana
```
python3 TBAFormatting.py -p HumanFrenchGuiana
```

Italian Wine 1
```
python3 TBAFormatting.py -p ItalianWine1
```

Italian Wine 2
```
python3 TBAFormatting.py -p ItalianWine2
```

Malaysian
```
python3 TBAFormatting.py -p Malaysian
```

Mediterranean Oak
```
python3 TBAFormatting.py -p MediterraneanOak
```

Mezcal
```
python3 TBAFormatting.py -p Mezcal
```

North American Oak
```
python3 TBAFormatting.py -p NorthAmericanOak
```

Sake
```
python3 TBAFormatting.py -p Sake
```

#### Run all_bz to create blastz alignments commands:

African Beer
```
cd AfricanBeer; all_bz - "(AEN_3 AFL_5 ANL_2 ANM_2 ASC_6 AVN_5 AVP_5 AVQ_5 AVR_6 AVS_5 AVT_6 BEI_7 BEK_7 BEM_7 BEN_7 BHA_2 R64)" >&all_bz.AfricanBeer.log; cd ..;
```

African Cocoa
```
cd AfricanCocoa; all_bz - "(ANN_4 CQC_6 CQE_6 CQG_6 CQI_6 CQL_4 CQN_4 CQD_6 CQF_6 CQH_6 CQK_4 CQM_4 CQP_4 R64)" >&all_bz.AfricanCocoa.log; cd ..;
```

African Palm Wine
```
cd AfricanPalmWine; all_bz - "(ADV_4 AKB_2 AKC_4 AKD_2 AKE_3 AKG_3 AKH_3 APQ_4 AVV_8 BAA_6 BAB_8 BAC_8 BAD_8 BAE_6 BAF_6 CFA_4 CPS_4 CPT_4 CQA_6 CQB_6 YAC R64)" >&all_bz.AfricanPalmWine.log; cd ..;
```

Ale Beer
```
cd AleBeer; all_bz - "(AAC_6 AEB_8 AEC_8 AGB_1 AQT_4 ASH_3 ATV_5 BRP_4 BRQ_4 BSI_3 CBR_2 CFC_2 CFD_4 CFG_4 CFH CFM_4 R64)" >&all_bz.AleBeer.log; cd ..;
```

Alpechin
```
cd Alpechin; all_bz - "(ABM_5 AHM_1 AQA_4 ATQ_7 AHL_3 AQD_4 BRK_4 R64)" >&all_bz.Alpechin.log; cd ..;
```

Asian Rice Fermentation
```
cd AsianRiceFermentation; all_bz - "(BEF_7 BEH_7 BHP_4 BHQ_4 BHR_4 BHS_1 CGN_5 CGR_5 R64)" >&all_bz.AsianRiceFermentation.log; cd ..;
```

Bioethanol
```
cd Bioethanol; all_bz - "(AEG_8 AEH_3 AFD_4 AFR_5 AGM_1 AHC_2 APG_4 BVB_6 BVD_5 BVF_5 BVH_1 CNB_4 CNE_4 CNG_4 CNI_4 CNL_4 CNN_1 CNQ_1 CNS_4 CNV_4 BVA_4 BVC_6 BVE_4 BVG_4 CMT_8 CNF_4 CNH_4 CNK_4 CNM_1 CNP_1 CNR_1 CNT_4 R64)" >&all_bz.Bioethanol.log; cd ..;
```

Dairy
```
cd Dairy; all_bz - "(AQM_4 ARR_6 ASN_8 BFH_3 BFS_8 BFT_8 BFV_8 BGA_7 BGB_7 BGC_8 BGD_8 BGE_8 BGF_8 BGG_8 BGH_1 BGI_1 BGK_1 BGL_1 BGM_1 BGN_1 BGP_1 BGQ_1 BGR_1 BGS_1 BGT_2 BGV_2 R64)" >&all_bz.Dairy.log; cd ..;
```

Ecuadorian
```
cd Ecuadorian; all_bz - "(CCC_3 CCD_4 CCE_6 CCF_6 CCG_4 CCH_4 CCI_4 R64)" >&all_bz.Ecuadorian.log; cd ..;
```

FarEastAsian
```
cd FarEastAsian; all_bz - "(BAI_7 BAK_7 CCN_1 CCQ_1 CCR_1 CCS_1 CDL_3 YCR R64)" >&all_bz.FarEastAsian.log; cd ..;
```

Human French Guiana
```
cd HumanFrenchGuiana; all_bz - "(BCB_3 BCD_8 BCF_8 BCH_5 BCK_8 BCM_8 BCP_8 BDP_5 BDR_6 BDT_6 BEA_6 BEC_6 BEE_6 BCC_3 BCE_8 BCG_8 BCI_8 BCL_5 BCN_8 BCQ_8 BDQ_6 BDS_6 BDV_6 BEB_6 BED_6 R64)" >&all_bz.HumanFrenchGuiana.log; cd ..;
```

Italian Wine 1
```
cd ItalianWine1; all_bz - "(AIA_1 AIR_1 APT_7 BHV_1 BIG_1 BIR_2 BKG_3 BKL_1 BNF_4 BPC_4 BPP_4 BQE_4 BQI_4 BQS_4 BRF_4 CRC_2 CRI_2 YBY AIN_1 AIT_2 ASR_1 BIF_1 BIM_3 BKE_1 BKH_1 BLV_3 BNG_4 BPL_2 BQB_4 BQH_4 BQM_4 BRE_4 BVK_2 CRD_2 CRK_2 YBX R64)" >&all_bz.ItalianWine1.log; cd ..;
```

Italian Wine 2
```
cd ItalianWine2; all_bz - "(ADR_4 AIF_1 AIP_2 BIA_1 BIC_1 BIP_3 BIS_2 BKF_3 BKN_2 BNE_2 BPM_4 BQG_4 BQQ_4 BRB_4 BSL_4 BSN_5 CQV_4 CRB_2 AGI_1 AIM_2 BAV_7 BIB_1 BIN_3 BIQ_3 BIT_2 BKM_1 BKP_1 BPB_4 BPT_5 BQP_4 BQT_4 BRC_4 BSM_3 BVM_3 CRA_2 R64)" >&all_bz.ItalianWine2.log; cd ..;
```

Malaysian
```
cd Malaysian; all_bz - "(AKM_4 BMA_2 BMB_1 BMC_1 MAL YCY R64)" >&all_bz.Malaysian.log; cd ..;
```

Mediterranean Oak
```
cd MediterraneanOak; all_bz - "(BBQ_3 BBS_3 BFP_3 BTG_3 CCL_1 CLB_5 CLC_4 CLD_5 R64)" >&all_bz.MediterraneanOak.log; cd ..;
```

Mezcal
```
cd Mezcal; all_bz - "(CPI_4, CPL_4, CPN_4, CPR_4, CPK_4, CPM_4, CPQ_4, R64)" >&all_bz.Mezcal.log; cd ..;
```

North American Oak
```
cd NorthAmericanOak; all_bz - "(ACD_7 AKN_3 ANC_4 AND_4 ANE_4 ANF_4 ANG_4 ANH_4 ANI_4 ANK_4 AVI_5 YBS YCU R64)" >&all_bz.NorthAmericanOak.log; cd ..;
```

Sake
```
cd Sake; all_bz - "(AAD_6 ADQ_4 AFM_5 ANR_2 ANT_2 APA_2 APD_4 APF_4 AVC_3 AVL_6 BTS_3 CLK_4 CLM_4 CLP_4 CLR_4 CLT_5 CMB_6 CMD_6 CMF_6 CMI_6 CML_8 CMN_8 CQR_4 ADN_4 AEF_8 AFN_4 ANS_2 ANV_5 APC_4 APE_4 AQN_7 AVD_3 AVM_5 CLI_4 CLL_4 CLN_4 CLQ_4 CLS_4 CLV_5 CMC_6 CME_6 CMH_6 CMK_6 CMM_8 CQQ_4 R64)" >&all_bz.Sake.log; cd ..;
```

#### Run TBA commands
African Beer
```
cd AfricanBeer; ./all_bz.AfricanBeer.log; cd ..;
```

African Cocoa
```
cd AfricanCocoa; ./all_bz.AfricanCocoa.log; cd ..;
```

African Palm Wine
```
cd AfricanPalmWine; ./all_bz.AfricanPalmWine.log; cd ..;
```

Ale Beer
```
cd AleBeer; ./all_bz.AleBeer.log; cd ..;
```

Alpechin
```
cd Alpechin; ./all_bz.Alpechin.log; cd ..;
```

Asian Rice Fermentation
```
cd AsianRiceFermentation; ./all_bz.AsianRiceFermentation.log; cd ..;
```

Bioethanol
```
cd Bioethanol; ./all_bz.Bioethanol.log; cd ..;
```

Dairy
```
cd Dairy; ./all_bz.Dairy.log; cd ..;
```

Ecuadorian
```
cd Ecuadorian; ./all_bz.Ecuadorian.log; cd ..;
```

Far East Asian
```
cd FarEastAsian; ./all_bz.FarEastAsian.log; cd ..;
```

Human French Guiana
```
cd HumanFrenchGuiana; ./all_bz.HumanFrenchGuiana.log; cd ..;
```

Italian Wine 1
```
cd ItalianWine1; ./all_bz.ItalianWine1.log; cd ..;
```

Italian Wine 2
```
cd ItalianWine2; ./all_bz.ItalianWine2.log; cd ..;
```

Malaysian
```
cd Malaysian; ./all_bz.Malaysian.log; cd ..;
```

Mediterranean Oak
```
cd MediterraneanOak; ./all_bz.MediterraneanOak.log; cd ..;
```

Mezcal
```
cd Mezcal; ./all_bz.Mezcal.log; cd ..;
```

North American Oak
```
cd NorthAmericanOak; ./all_bz.NorthAmericanOak.log; cd ..;
```

Sake
```
cd Sake; ./all_bz.Sake.log; cd ..;
```

#### Run tba to compile the generated pairwise alignments files into a single multiple alignment file:

African Beer
```
cd AfricanBeer; tba "(AEN_3 AFL_5 ANL_2 ANM_2 ASC_6 AVN_5 AVP_5 AVQ_5 AVR_6 AVS_5 AVT_6 BEI_7 BEK_7 BEM_7 BEN_7 BHA_2 R64)" *.*.maf AfricanBeer.maf >&AfricanBeer_tba.log; cd ..;
```
African Cocoa
```
cd AfricanCocoa; tba "(ANN_4 CQC_6 CQE_6 CQG_6 CQI_6 CQL_4 CQN_4 CQD_6 CQF_6 CQH_6 CQK_4 CQM_4 CQP_4 R64)" *.*.maf AfricanCocoa.maf >&AfricanCocoa_tba.log; cd ..;
```

African Palm Wine
```
cd AfricanPalmWine; tba "(ADV_4 AKB_2 AKC_4 AKD_2 AKE_3 AKG_3 AKH_3 APQ_4 AVV_8 BAA_6 BAB_8 BAC_8 BAD_8 BAE_6 BAF_6 CFA_4 CPS_4 CPT_4 CQA_6 CQB_6 YAC R64)" *.*.maf AfricanPalmWine.maf >&AfricanPalmWine_tba.log; cd ..;
```

Ale Beer
```
cd AleBeer; tba "(AAC_6 AEB_8 AEC_8 AGB_1 AQT_4 ASH_3 ATV_5 BRP_4 BRQ_4 BSI_3 CBR_2 CFC_2 CFD_4 CFG_4 CFH CFM_4 R64)" *.*.maf AleBeer.maf >&AleBeer_tba.log; cd ..;
```

Alpechin
```
cd Alpechin; tba "(ABM_5 AHM_1 AQA_4 ATQ_7 AHL_3 AQD_4 BRK_4 R64)" *.*.maf Alpechin.maf >&AleBeer_tba.log; cd ..;
```

Asian Rice Fermentation
```
cd AsianRiceFermentation; tba "(BEF_7 BEH_7 BHP_4 BHQ_4 BHR_4 BHS_1 CGN_5 CGR_5 R64)" *.*.maf AsianRiceFermentation.maf >&AsianRiceFermentation_tba.log; cd ..;
```

Bioethanol
```
cd Bioethanol; tba "(AEG_8 AEH_3 AFD_4 AFR_5 AGM_1 AHC_2 APG_4 BVB_6 BVD_5 BVF_5 BVH_1 CNB_4 CNE_4 CNG_4 CNI_4 CNL_4 CNN_1 CNQ_1 CNS_4 CNV_4 BVA_4 BVC_6 BVE_4 BVG_4 CMT_8 CNF_4 CNH_4 CNK_4 CNM_1 CNP_1 CNR_1 CNT_4 R64)" *.*.maf Bioethanol.maf >&Bioethanol_tba.log; cd ..;
```

Dairy
```
cd Dairy; tba "(AQM_4 ARR_6 ASN_8 BFH_3 BFS_8 BFT_8 BFV_8 BGA_7 BGB_7 BGC_8 BGD_8 BGE_8 BGF_8 BGG_8 BGH_1 BGI_1 BGK_1 BGL_1 BGM_1 BGN_1 BGP_1 BGQ_1 BGR_1 BGS_1 BGT_2 BGV_2 R64)" *.*.maf Dairy.maf >&Dairy_tba.log; cd ..;
```

Ecuadorian
```
cd Ecuadorian; tba "(CCC_3 CCD_4 CCE_6 CCF_6 CCG_4 CCH_4 CCI_4 R64)" *.*.maf Ecuadorian.maf >&Ecuadorian_tba.log; cd ..;
```

Far East Asian
```
cd FarEastAsian; tba "(BAI_7 BAK_7 CCN_1 CCQ_1 CCR_1 CCS_1 CDL_3 YCR R64)" *.*.maf FarEastAsian.maf >&FarEastAsian_tba.log; cd ..;
```

Human French Guiana
```
cd HumanFrenchGuiana; tba "(BCB_3 BCD_8 BCF_8 BCH_5 BCK_8 BCM_8 BCP_8 BDP_5 BDR_6 BDT_6 BEA_6 BEC_6 BEE_6 BCC_3 BCE_8 BCG_8 BCI_8 BCL_5 BCN_8 BCQ_8 BDQ_6 BDS_6 BDV_6 BEB_6 BED_6 R64)" *.*.maf HumanFrenchGuiana.maf >&HumanFrenchGuiana_tba.log; cd ..;
```

Italian Wine 1
```
cd ItalianWine1; tba "(AIA_1 AIR_1 APT_7 BHV_1 BIG_1 BIR_2 BKG_3 BKL_1 BNF_4 BPC_4 BPP_4 BQE_4 BQI_4 BQS_4 BRF_4 CRC_2 CRI_2 YBY AIN_1 AIT_2 ASR_1 BIF_1 BIM_3 BKE_1 BKH_1 BLV_3 BNG_4 BPL_2 BQB_4 BQH_4 BQM_4 BRE_4 BVK_2 CRD_2 CRK_2 YBX R64)" *.*.maf ItalianWine1.maf >&ItalianWine1_tba.log; cd ..;
```

Italian Wine 2
```
cd ItalianWine2; tba "(ADR_4 AIF_1 AIP_2 BIA_1 BIC_1 BIP_3 BIS_2 BKF_3 BKN_2 BNE_2 BPM_4 BQG_4 BQQ_4 BRB_4 BSL_4 BSN_5 CQV_4 CRB_2 AGI_1 AIM_2 BAV_7 BIB_1 BIN_3 BIQ_3 BIT_2 BKM_1 BKP_1 BPB_4 BPT_5 BQP_4 BQT_4 BRC_4 BSM_3 BVM_3 CRA_2 R64)" *.*.maf ItalianWine2.maf >&ItalianWine2_tba.log; cd ..;
```

Malaysian
```
cd Malaysian; tba "(AKM_4 BMA_2 BMB_1 BMC_1 MAL YCY R64)" *.*.maf Malaysian.maf >&Malaysian_tba.log; cd ..;
```

Mediterranean Oak
```
cd MediterraneanOak; tba "(BBQ_3 BBS_3 BFP_3 BTG_3 CCL_1 CLB_5 CLC_4 CLD_5 R64)" *.*.maf MediterraneanOak.maf >&MediterraneanOak_tba.log; cd ..;
```

Mezcal
```
cd Mezcal; tba "(CPI_4, CPL_4, CPN_4, CPR_4, CPK_4, CPM_4, CPQ_4, R64)" *.*.maf Mezcal.maf >&Mezcal_tba.log; cd ..;
```

North American Oak
```
cd NorthAmericanOak; tba "(ACD_7 AKN_3 ANC_4 AND_4 ANE_4 ANF_4 ANG_4 ANH_4 ANI_4 ANK_4 AVI_5 YBS YCU R64)" *.*.maf NorthAmericanOak.maf >&NorthAmericanOak_tba.log; cd ..;
```

Sake
```
cd Sake; tba "(AAD_6 ADQ_4 AFM_5 ANR_2 ANT_2 APA_2 APD_4 APF_4 AVC_3 AVL_6 BTS_3 CLK_4 CLM_4 CLP_4 CLR_4 CLT_5 CMB_6 CMD_6 CMF_6 CMI_6 CML_8 CMN_8 CQR_4 ADN_4 AEF_8 AFN_4 ANS_2 ANV_5 APC_4 APE_4 AQN_7 AVD_3 AVM_5 CLI_4 CLL_4 CLN_4 CLQ_4 CLS_4 CLV_5 CMC_6 CME_6 CMH_6 CMK_6 CMM_8 CQQ_4 R64)" *.*.maf Sake.maf >&Sake_tba.log; cd ..;
```


#### Project alignments onto reference genome:

African Beer
```
cd AfricanBeer; maf_project AfricanBeer.maf R64 > AfricanBeer_R64.maf; cd ..;
```

African Cocoa
```
cd AfricanCocoa; maf_project AfricanCocoa.maf R64 > AfricanCocoa_R64.maf; cd ..;
```

African Palm Wine
```
cd AfricanPalmWine; maf_project AfricanPalmWine.maf R64 > AfricanPalmWine_R64.maf; cd ..;
```

Ale Beer
```
cd AleBeer; maf_project AleBeer.maf R64 > AleBeer_R64.maf; cd ..;
```

Alpechin
```
cd Alpechin; maf_project Alpechin.maf R64 > Alpechin_R64.maf; cd ..;
```

Asian Rice Fermentation
```
cd AsianRiceFermentation; maf_project AsianRiceFermentation.maf R64 > AsianRiceFermentation_R64.maf; cd ..;
```

Bioethanol
```
cd Bioethanol; maf_project Bioethanol.maf R64 > Bioethanol_R64.maf; cd ..;
```

Dairy
```
cd Dairy; maf_project Dairy.maf R64 > Dairy_R64.maf; cd ..;
```

Ecuadorian
```
cd Ecuadorian; maf_project Ecuadorian.maf R64 > Ecuadorian_R64.maf; cd ..;
```

Far East Asian
```
cd FarEastAsian; maf_project FarEastAsian.maf R64 > FarEastAsian_R64.maf; cd ..;
```

Human French Guiana
```
cd HumanFrenchGuiana; maf_project HumanFrenchGuiana.maf R64 > HumanFrenchGuiana_R64.maf; cd ..;
```

Italian Wine 1
```
cd ItalianWine1; maf_project ItalianWine1.maf R64 > ItalianWine1_R64.maf; cd ..;
```

Italian Wine 2
```
cd ItalianWine2; maf_project ItalianWine2.maf R64 > ItalianWine2_R64.maf; cd ..;
```

Malaysian
```
cd Malaysian; maf_project Malaysian.maf R64 > Malaysian_R64.maf; cd ..;
```

Mediterranean Oak
```
cd MediterraneanOak; maf_project MediterraneanOak.maf R64 > MediterraneanOak_R64.maf; cd ..;
```

Mezcal
```
cd Mezcal; maf_project Mezcal.maf R64 > Mezcal_R64.maf; cd ..;
```

North American Oak
```
cd NorthAmericanOak; maf_project NorthAmericanOak.maf R64 > NorthAmericanOak_R64.maf; cd ..;
```

Sake
```
cd Sake; maf_project Sake.maf R64 > Sake_R64.maf; cd ..;
```

#### Zip alignment files and store in AlignmentProcessing/ directory

```
gzip *_R64.maf
```

```
mv *_R64.maf.gz ../AlignmentProcessing/AlignmentFiles/
```
