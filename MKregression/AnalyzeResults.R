# Read all MK results and make a table
parse.MKresults <- function(populations, type) {
  require(data.table)
  lst <- list()
  for (pop in populations) {
    cat("Reading results for", pop, "\n")
    tmp <- read.table(paste0(pop, "_MKresults", type, ".tsv"), sep ="\t", header = TRUE)
    tmp$Population <- pop
    lst[[pop]] <- tmp                  
  }
  dat <- rbindlist(lst)
  return(dat)
}

populations <- c("AfricanBeer",
                 "AfricanCocoa",
                 "AfricanPalmWine",
                 "AleBeer",
                 "Alpechin",
                 "AsianRiceFermentation",
                 "Bioethanol",
                 "Dairy",
                 "Ecuadorian",
                 "FarEastAsian",
                 "HumanFrenchGuiana",
                 "Malaysian",
                 "ItalianWine1",
                 "ItalianWine2",
                 "Malaysian",
                 "MediterraneanOak",
                 "NorthAmericanOak",
                 "Sake")
dat.all <- parse.MKresults(populations, "")
View(subset(na.omit(dat.all), p.value < 0.1))

dat.reduced <- parse.MKresults(populations, "_reduced")
View(subset(na.omit(dat.reduced), p.value < 0.1))

dat.veryreduced <- parse.MKresults(populations, "_veryreduced")
View(subset(na.omit(dat.veryreduced), p.value < 0.1))

dat.geneset <- parse.MKresults(populations, "_geneset")
View(subset(na.omit(dat.geneset), p.value < 0.1))

dat.rsa <- parse.MKresults(populations, "_rsa")
View(subset(na.omit(dat.rsa), p.value < 0.1))

dat.co <- parse.MKresults(populations, "_COrate")
View(subset(na.omit(dat.co), p.value < 0.1))
