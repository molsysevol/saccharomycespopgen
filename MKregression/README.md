The MKregression script was retrieved from https://github.com/yifei-lab/MK-regression/ on 08/01/22 and was used without modification.
The `FormatData.R` R script is used to generate input files for each population.

Perform the MK regression:
```bash
run_mkreg() {
  python3.9 MKRegression.py -n ${1}_4D_sites${2}.tsv  -f ${1}_0D_sites${2}.tsv -p ${1}_MKresults${2}.tsv > ${1}_MKlog_likelihood${2}.txt
}

run_all_mkreg() {
  run_mkreg $1
  run_mkreg $1 _reduced
  run_mkreg $1 _veryreduced
  run_mkreg $1 _geneset
  run_mkreg $1 _rsa
  run_mkreg $1 _COrate
}

run_all_mkreg AfricanBeer
run_all_mkreg AfricanCocoa
run_all_mkreg AfricanPalmWine
run_all_mkreg AleBeer
run_all_mkreg Alpechin
run_all_mkreg AsianRiceFermentation
run_all_mkreg Bioethanol
run_all_mkreg Dairy
run_all_mkreg Ecuadorian
run_all_mkreg FarEastAsian
run_all_mkreg HumanFrenchGuiana
run_all_mkreg ItalianWine1
run_all_mkreg ItalianWine2
run_all_mkreg Malaysian
run_all_mkreg MediterraneanOak
run_all_mkreg NorthAmericanOak
run_all_mkreg Sake
```

Analysis of results is in `AnalyzeResults.R`.

Globally, coefficients go in the same direction as the Grapes analyses, but results are not significant in most cases. Joint variable analyses lead to unfitable models in all cases except the Malaysian population. So globally very low signal with this approach, due to a generally low omega_A and/or small sample size.
