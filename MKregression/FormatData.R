# Read popstat output and generate input file for the MKregression
geneset.dc <- read.table("../GeneSets/DomesticCarbon/DomesticCarbonIndep", header = TRUE, stringsAsFactors = FALSE)$V1
geneset.ms <- read.table("../GeneSets/MetalStress/MetalStressIndep", header = TRUE, stringsAsFactors = FALSE)$V1
geneset.xs <- read.table("../GeneSets/OxidativeStress/OxidativeStressIndep", header = TRUE, stringsAsFactors = FALSE)$V1
geneset.hs <- read.table("../GeneSets/HeatStress/HeatStressIndep", header = TRUE, stringsAsFactors = FALSE)$V1
geneset.os <- read.table("../GeneSets/OsmoticStress/OsmoticStressIndep", header = TRUE, stringsAsFactors = FALSE)$V1
geneset.me <- read.table("../GeneSets/Meiosis/MeiosisIndep", header = TRUE, stringsAsFactors = FALSE)$V1
geneset.mu <- read.table("../GeneSets/MultipleSets/MultipleSetsIndep", header = TRUE, stringsAsFactors = FALSE)$V1

read.data <- function(pop) {
  require(data.table)
  files <- list.files(path = paste0("../PopStats/", pop, "/CodonStats/"), pattern = "*.codons.csv")
  lst <- list()
  pb <- txtProgressBar(0, length(files), style = 3)
  for (i in 1:length(files)) {
    setTxtProgressBar(pb, i)
    #cat(files[i], "\n")
    lst[[files[i]]] <- read.table(paste0("../PopStats/", pop, "/CodonStats/", files[i]), header = TRUE, sep = "\t", stringsAsFactors = FALSE)
  }
  df <- rbindlist(lst, fill = TRUE)
  
  df$DomesticCarbon <- as.factor(as.numeric(df$Gene %in% geneset.dc))
  df$MetalStress <- as.factor(as.numeric(df$Gene %in% geneset.ms))
  df$OxidativeStress <- as.factor(as.numeric(df$Gene %in% geneset.xs))
  df$HeatStress	<- as.factor(as.numeric(df$Gene %in% geneset.hs))
  df$OsmoticStress <- as.factor(as.numeric(df$Gene %in% geneset.os))
  df$Meiosis <- as.factor(as.numeric(df$Gene %in% geneset.me))
  df$MultipleCategories <- as.factor(as.numeric(df$Gene %in% geneset.mu))
  df$Stress <- as.factor(as.numeric(df$Gene %in% c(geneset.ms, geneset.xs, geneset.hs, geneset.os)))

  return(df)
}

get.fold <- function(codon, pos, gencode = 1) {
  require(seqinr)
  codon <- tolower(codon)
  # Get all synonymous codons:
  syn <- lapply(syncodons(codon, numcode = gencode)[[1]], s2c)
  # Keep the ones that have the same non-focal positions:
  syn2 <- syn[sapply(syn, function(x) return(all(x[-pos] == s2c(codon)[-pos])))]
  if (length(syn2) == 1) return(0)
  if (length(syn2) == 4) return(4)
  if (length(syn2) == 6) return(6)
  return(2)
}

# Precompute folds for all positions of all codons:
library(seqinr)
nt <- c("a", "c", "g", "t")
codons <- character(64)
fold1 <- numeric(64)
fold2 <- numeric(64)
fold3 <- numeric(64)
i <- 0
for (x1 in nt) {
  for (x2 in nt) {
    for (x3 in nt) {
      i <- i + 1
      codon <- c2s(c(x1, x2, x3))
      codons[i] <- codon
      fold1[i] <- get.fold(codon, 1)
      fold2[i] <- get.fold(codon, 2)
      fold3[i] <- get.fold(codon, 3)
    }
  }
}
folds <- data.frame(row.names = codons, pos1 = fold1, pos2 = fold2, pos3 = fold3)
apply(folds, 2, mean) # Looks good

# Now compute at which position the mutation occurred:
get.mutation.pos <- function(codon1, codon2) {
  p <- which(s2c(tolower(codon1)) != s2c(tolower(codon2)))
  if (length(p) == 0) return(0)
  if (length(p) > 1) return(4)
  return(p)
}
get.mutation.pos("AAC", "ATC")
get.mutation.pos("AAC", "AAC")
get.mutation.pos("AGG", "ATC")


get.mk.data <- function(dat, folds, variables) {
  require(plyr)
  cols <- c(variables, c("Site", "Gene", "AncestralAllele", "OutgroupAllele", "MajorAllele", "MinorAllele", "MinorAlleleFrequency", "MajorAlleleFrequency"))
  dat.clean1 <- na.omit(dat[,..cols])
  cat("Clean data set has ", nrow(dat.clean1), " rows and ", ncol(dat.clean1), " columns.\n", sep = "")
  dat.clean1$DerivedAlleleFrequency <- with(dat.clean1, ifelse(MajorAllele == AncestralAllele, MinorAlleleFrequency, MajorAlleleFrequency)/ifelse(MajorAllele == MinorAllele, MajorAlleleFrequency, (MajorAlleleFrequency + MinorAlleleFrequency)))
  hist(dat.clean1$DerivedAlleleFrequency)
  cat("Computing folds for each position...\n")
  dat.clean1$FoldPos1 <- folds[tolower(dat.clean1$AncestralAllele), "pos1"]
  dat.clean1$FoldPos2 <- folds[tolower(dat.clean1$AncestralAllele), "pos2"]
  dat.clean1$FoldPos3 <- folds[tolower(dat.clean1$AncestralAllele), "pos3"]
  cat("Computing positions of mutations...\n")
  dat.clean2 <- ddply(dat.clean1, .variables = c("Site", "Gene"), plyr::summarize,
                         MutPosPol = get.mutation.pos(MinorAllele, MajorAllele),
                         MutPosDiv = get.mutation.pos(AncestralAllele, OutgroupAllele))
  dat.clean3 <- merge(dat.clean1, dat.clean2, by = c("Site", "Gene"))
  
  cat("Splitting sites into 0D and 4D...\n")
  dat.neut <- subset(dat.clean3, FoldPos3 == 4 & MutPosPol < 4 & MutPosDiv < 4)
  dat.neut$poly_label <- with(dat.neut, ifelse(MutPosPol == 3, 1, 0))
  dat.neut$div_label <- with(dat.neut, ifelse(MutPosDiv == 3, 1, 0))
  
  dat.func <- subset(dat.clean3, (FoldPos1 == 0 | FoldPos2 == 0 | FoldPos3 == 0) & MutPosPol < 4 & MutPosDiv < 4)
  dat.func$poly_label <- with(dat.func, ifelse((MutPosPol == 1 & FoldPos1 == 0) | (MutPosPol == 2 & FoldPos2 == 0) | (MutPosPol == 3 & FoldPos3 == 0), 1, 0))
  dat.func$div_label <- with(dat.func, ifelse((MutPosDiv == 1 & FoldPos1 == 0) | (MutPosDiv == 2 & FoldPos2 == 0) | (MutPosDiv == 3 & FoldPos3 == 0), 1, 0))
  
  return(list(functional = dat.func, neutral = dat.neut))
}

write.mkfiles <- function(dat.mk, out.func, out.neut, variables, minfreq = 0) {
  require(plyr)
  cat("Writing results to files...\n")
  cols <- c(c("div_label", "poly_label"), variables)
  write.table(dat.mk$functional[DerivedAlleleFrequency >= minfreq, ..cols], file = out.func, sep = "\t", row.names = FALSE, quote = FALSE)
  write.table(dat.mk$neutral[DerivedAlleleFrequency >= minfreq, c("div_label", "poly_label")], file = out.neut, sep = "\t", row.names = FALSE, quote = FALSE)
  cat("Done.\n")
}

variables.full <- c("RSA", "CORate", "DomesticCarbon", "MetalStress", "OxidativeStress", "HeatStress", "OsmoticStress", "Meiosis", "MultipleCategories")
variables.reduced <- c("RSA", "CORate", "DomesticCarbon", "Stress", "Meiosis")
variables.veryreduced <- c("RSA", "CORate")
variables.geneset <- c("DomesticCarbon", "MetalStress", "OxidativeStress", "HeatStress", "OsmoticStress", "Meiosis", "MultipleCategories")

generate.mkdata <- function(pop) {
  dat <- read.data(pop)
  dat.mk.full <- get.mk.data(dat, folds = folds,
                             variables = variables.full)
  write.mkfiles(dat.mk.full,
                out.func = paste0(pop, "_0D_sites.tsv"),
                out.neut = paste0(pop, "_4D_sites.tsv"),
                variables = variables.full, minfreq = 0.5)
  
  dat.mk.reduced <- get.mk.data(dat, folds = folds,
                                variables = variables.reduced)
  write.mkfiles(dat.mk.reduced,
                out.func = paste0(pop, "_0D_sites_reduced.tsv"),
                out.neut = paste0(pop, "_4D_sites_reduced.tsv"),
                variables = variables.reduced, minfreq = 0.5)
  
  dat.mk.veryreduced <- get.mk.data(dat, folds = folds,
                                    variables = variables.veryreduced)
  write.mkfiles(dat.mk.veryreduced,
                out.func = paste0(pop, "_0D_sites_veryreduced.tsv"),
                out.neut = paste0(pop, "_4D_sites_veryreduced.tsv"),
                variables = variables.veryreduced, minfreq = 0.5)
  
  dat.mk.geneset <- get.mk.data(dat, folds = folds,
                                variables = variables.geneset)
  write.mkfiles(dat.mk.geneset,
                out.func = paste0(pop, "_0D_sites_geneset.tsv"),
                out.neut = paste0(pop, "_4D_sites_geneset.tsv"),
                variables = variables.geneset, minfreq = 0.5)

  dat.mk.rsa <- get.mk.data(dat, folds = folds,
                                 variables = "RSA")
  write.mkfiles(dat.mk.rsa,
                out.func = paste0(pop, "_0D_sites_rsa.tsv"),
                out.neut = paste0(pop, "_4D_sites_rsa.tsv"),
                variables = "RSA", minfreq = 0.5)
  
  dat.mk.co <- get.mk.data(dat, folds = folds,
                            variables = "CORate")
  write.mkfiles(dat.mk.co,
                out.func = paste0(pop, "_0D_sites_COrate.tsv"),
                out.neut = paste0(pop, "_4D_sites_COrate.tsv"),
                variables = "CORate", minfreq = 0.5)
  
  return(dat)
}

dat.ab <- generate.mkdata("AfricanBeer")
summary(dat.ab[, ..variables.full])
summary(dat.ab[, ..variables.reduced])

dat.ac <- generate.mkdata("AfricanCocoa")
dat.ap <- generate.mkdata("AfricanPalmWine")
dat.ab <- generate.mkdata("AleBeer")
dat.al <- generate.mkdata("Alpechin")
dat.ar <- generate.mkdata("AsianRiceFermentation")
dat.be <- generate.mkdata("Bioethanol")
dat.da <- generate.mkdata("Dairy")
dat.ec <- generate.mkdata("Ecuadorian")
dat.fe <- generate.mkdata("FarEastAsian")
dat.hg <- generate.mkdata("HumanFrenchGuiana")
dat.i1 <- generate.mkdata("ItalianWine1")
dat.i2 <- generate.mkdata("ItalianWine2")
dat.ma <- generate.mkdata("Malaysian")
dat.mo <- generate.mkdata("MediterraneanOak")
dat.no <- generate.mkdata("NorthAmericanOak")
dat.sa <- generate.mkdata("Sake")


# The effect of gene sets change whether we include RSA and CORate. We assess the link between these variales:
dat.ma[!is.na(dat.ma$Gene), "GeneSet"] <- "Other"
dat.ma[dat.ma$Gene %in% geneset.dc, "GeneSet"] <- "Domestic carbon"
dat.ma[dat.ma$Gene %in% geneset.hs, "GeneSet"] <- "Heat stress"
dat.ma[dat.ma$Gene %in% geneset.me, "GeneSet"] <- "Meiosis"
dat.ma[dat.ma$Gene %in% geneset.ms, "GeneSet"] <- "Metal stress"
dat.ma[dat.ma$Gene %in% geneset.mu, "GeneSet"] <- "Multiple categories"
dat.ma[dat.ma$Gene %in% geneset.os, "GeneSet"] <- "Osmotic stress"
dat.ma[dat.ma$Gene %in% geneset.xs, "GeneSet"] <- "Oxydative stress"

library(ggplot2)
p <- ggplot(dat.ma, aes(y=RSA, x=GeneSet)) + geom_boxplot()
p
p <- ggplot(dat.ma, aes(y=CORate, x=GeneSet)) + geom_boxplot()
p

library(agricolae)
kruskal(y = dat.ma$RSA, trt = dat.ma$GeneSet, group = TRUE, console = TRUE, alpha = 0.01)
#Treatments with the same letter are not significantly different.
#
#dat.ma$RSA groups
#Osmotic stress        867679.1      a
#Heat stress           823639.6      b
#Multiple categories   805913.3      c
#Meiosis               797508.8      c
#Metal stress          756785.9      d
#Other                 749213.6      e
#Oxydative stress      748045.6      e
#Domestic carbon       745931.4      e

kruskal(y = dat.ma$CORate, trt = dat.ma$GeneSet, group = TRUE, console = TRUE, alpha = 0.01)
#Treatments with the same letter are not significantly different.
#
#dat.ma$CORate groups
#Multiple categories      749850.5      a
#Metal stress             706529.8      b
#Oxydative stress         705547.5      b
#Osmotic stress           698865.6      b
#Other                    658738.5      c
#Domestic carbon          648897.1      d
#Meiosis                  587061.1      e
#Heat stress              545560.9      f
