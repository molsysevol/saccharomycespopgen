#! /bin/python3

"""
Reads a list of commands from a file and generates a SLURM script to run them in parallel
"""

import sys
"""
Args are 'input file' 'job name' 'email' 'nlines'
"""
with open(sys.argv[1]) as file:
    lines = [line.rstrip() for line in file]

    print("#!/bin/sh")
    print("#SBATCH --job-name=%s" % sys.argv[2])
    print("#SBATCH --array=1-" + str(round(len(lines) / int(sys.argv[4]))))
    print("#SBATCH --output=" + sys.argv[2] + "%J.out")
    print("#SBATCH --error=" + sys.argv[2] + "%J.err")
    print("#SBATCH --ntasks=1")
    print("#SBATCH --nodes=1")
    print("#SBATCH --time=48:00:00")
    print("#SBATCH --mem=5G")
    print("#SBATCH --mail-type=ALL")
    print("#SBATCH --mail-user=%s" % sys.argv[3])
    print("#SBATCH --partition=global")
    print()
    count = 0
    task = 1
    for i, line in enumerate(lines):
        if (count == 0) :
            print("if [ $SLURM_ARRAY_TASK_ID == %s ]; then" % task)
        print("  " + line)
        count = count + 1
        if (count == int(sys.argv[4]) or i == len(lines)) :
            print("fi")
            count = 0
            task = task + 1


