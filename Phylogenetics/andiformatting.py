import glob
from Bio import SeqIO

genomes = list(glob.glob("./TBAFormatted/*"))

for genome in genomes:
    formattedgenome = list()
    parsedgenome = SeqIO.parse(genome, "fasta")
    genome = genome.split("/")[-1]
    for seq_record in parsedgenome:
        seq_record.id = genome
        seq_record.description = ""
        seq_record.name = ""
        formattedgenome.append(seq_record)
    outfile = "./andiFormatted/" + genome + ".fasta"
    SeqIO.write(formattedgenome, outfile, "fasta")

