# Creating phylogenetic tree
# Created on 19/12/2021 by Maximilian Raas

#### Create directories:
```
mkdir TreeFiles
```

```
mkdir TBAFormatted
```

```
mkdir andiFormatted
```

#### Prepare fasta files for andi:

Move TBA-formatted fasta files for isolates to be included in the phylogenetic analysis in directory TBAFormatted

Format fasta files for andi
```
python3 andiformatting.py
```

#### Run andi:

```
cd andiFormatted; andi --join *.fasta > ../TreeFiles/FinalTree.mat; cd ..
```

#### Run FastME:

Format andi output file for fastme

```
Rscript fastmeformat.R
```

Run FastME

```
cd TreeFiles; fastme -i FinalTree.fixed.mat -o FinalTree; cd ..
```

