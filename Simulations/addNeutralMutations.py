import subprocess, msprime, tskit, numpy, pandas, sys, random, warnings, gzip

# see: https://github.com/tskit-dev/pyslim/issues/168
def make_alleles(n):
    nucs = ['A', 'C', 'G', 'T']
    alleles = nucs.copy()
    numpy.random.shuffle(alleles)
    while len(alleles) < n:
        new = [a + b for a in alleles for b in nucs]
        numpy.random.shuffle(new)
        alleles.extend(new)
    return alleles[:n]

if __name__ == "__main__":
    ts_path = sys.argv[1] + ".5indv.trees"
    print("Input tree sequence: %s\n" % ts_path)
    seed = int(sys.argv[2])
    print("Random seed: %i\n" % seed)
    numpy.random.seed(seed)

    ts = tskit.load(ts_path)

    mutation_model = msprime.SLiMMutationModel(0, 0)

    mts = msprime.sim_mutations(ts, rate = 1.7e-7, model = mutation_model, keep = True, random_seed = seed)

    t = mts.tables
    t.sites.clear()
    t.mutations.clear()


    for s in mts.sites():
        alleles = make_alleles(len(s.mutations) + 1)
        t.sites.append(s.replace(ancestral_state=alleles[0]))
        for j, m in enumerate(s.mutations):
            t.mutations.append(m.replace(derived_state=alleles[j+1][0])) # If there are more than 4 states, takes only first character fo 5th state 

    mts = t.tree_sequence()

    outfile = sys.argv[1] + ".5indv.vcf.gz"

    with gzip.open(outfile, "wt") as vcf_file:
        mts.write_vcf(vcf_file) #, position_transform = "legacy") # 'legacy' is used to avoid SNPs at the same position


