<!-- Created on 29.06.22 by jdutheil -->

Preamble
========

Retrieve gene coordinates:
```r
library(ape)
gff <- read.gff("../AlignmentProcessing/R64.gff3")
table(gff$type)
# Discard mitochondrion:
gff <- subset(gff,  seqid != "Mito")
exons <- subset(gff, type == "exon")
chromosomes <- subset(gff, type == "chromosome")

# Collapse overlapping exons:
library(GenomicRanges)
ranges <- makeGRangesFromDataFrame(exons, ignore.strand = TRUE) #7420
ranges <- reduce(ranges) #6658 (7226 not ignoring strand)
exons <- as.data.frame(ranges)

# Calculate offset positions to concatenate chromosomes:
chrs.idx <- chromosomes$end
names(chrs.idx) <- chromosomes$seqid
chrs.idx <- cumsum(chrs.idx)
cat("Genome length:", chrs.idx[length(chrs.idx)], "\n")
for (i in length(chrs.idx):2) {
  chrs.idx[i] <- chrs.idx[i - 1]
}
chrs.idx[1] <- 0
chromosomes2 <- data.frame(chr = chromosomes$seqid, begin = chrs.idx + 1, end =  chrs.idx + chromosomes$end)
write.table(chromosomes2, file = "Chromosomes.csv", row.names = FALSE, sep = ",")

library(plyr)
exons2 <- ddply(exons, .var = "seqnames", .fun = function(d) {
  x <- as.character(unique(d$seqnames))
  cat(x, chrs.idx[x], "\n")
  d$start <- d$start + chrs.idx[x]
  d$end <- d$end + chrs.idx[x]
  return(d)
})

exons3 <- exons2[order(exons2$start),]
write.table(exons3[,c("start", "end")], file = "Exons.csv", col.names = FALSE, row.names = FALSE, sep = ",")
```

Necessary script:
```bash
wget https://raw.githubusercontent.com/stschiff/msmc-tools/master/generate_multihetsep.py
```

Neutral simulations
===================

Simulate with msprime:
```batch
python3 simNeutralConstant.py 42
```

Cut into individual chromosomes:
```r
chrs <- read.csv("Chromosomes.csv")
dir.create("VcfSplit")
for (i in 1:10) {
  cat("Rep ", i, "...\n")
  dir.create(paste0("VcfSplit/Rep", i))
  for(j in 1:nrow(chrs)) {
    cat("- Chr ", chrs[j, "chr"], "...\n")
    dir.create(paste0("VcfSplit/Rep", i, "/Chr", chrs[j, "chr"]))
    for (k in 0:4) {
      cat("  + Individual ", k, "...\n")
      system(paste0("vcftools --gzvcf simSc_neutral_demoConstant_rep", i-1, ".5indv.vcf.gz --chr 1 --indv tsk_", k, " --from-bp ", chrs[j, "begin"], " --to-bp ", chrs[j, "end"], " --recode --stdout | bgzip -c > VcfSplit/Rep", i, "/Chr", chrs[j, "chr"], "/simSc_neutral_demoConstant_rep", i, ".indv", k, ".chr", chrs[j, "chr"], ".vcf.gz"))
    }
  }
}
```

Then generate MSMC input files. First without missing data:
```r
chrs <- read.csv("Chromosomes.csv")
for (i in 1:10) {
  cat("Rep ", i, "...\n")
  for(j in 1:nrow(chrs)) {
    cat("- Chr ", chrs[j, "chr"], "...\n")
    system(paste0("python3 generate_multihetsep.py --chr ", chrs[j, "chr"], " VcfSplit/Rep", i, "/Chr", chrs[j, "chr"], "/simSc_neutral_demoConstant_rep", i, ".indv*.vcf.gz > VcfSplit/Rep", i, "/simSc_neutral_demoConstant_rep", i, ".chr", chrs[j, "chr"], ".multihetsep.msmc"))
  }
}
```

Fix multiple SNPs:
```r
chrs <- read.csv("Chromosomes.csv")
for (i in 1:1) {
  cat("Rep ", i, "...\n")
  for(j in 1:nrow(chrs)) {
    cat("- Chr ", chrs[j, "chr"], "...\n")
    f <- paste0("VcfSplit/Rep", i, "/simSc_neutral_demoConstant_rep", i, ".chr", chrs[j, "chr"], ".multihetsep.msmc")
    print(f)
    system(paste0("gawk -i inplace -v INPLACE_SUFFIX=.bak '$3 == \"0\" { next } { print }' ", f))
  }
}
```
 
Finally run MSMC2:
```bash
for i in {1..10}; do
  echo "Rep $i..."
  msmc2 -o simSc_neutral_demoConstant_rep${i}.msmc VcfSplit/Rep$i/simSc_neutral_demoConstant*.multihetsep.msmc
done
```

```r
lst <- list()
for (i in 1:10) {
  d <- NULL
  try(d <- read.table(paste0("simSc_neutral_demoConstant_rep", i, ".msmc.final.txt"), header = TRUE))
  if (!is.null(d)) {
    d$replicate <- i
    lst[[i]] <- d
  }
} #4 replicates failed to estimate!
library(data.table)
dat <- rbindlist(lst)

library(ggplot2)
p <- ggplot(dat) + geom_step(aes(x = left_time_boundary, y = 1/lambda, group = replicate)) +
  scale_x_log10() + scale_y_log10()
p
```


Try to run with a distinct segment pattern:
```bash
for i in {1..10}; do
  echo "Rep $i..."
  msmc2 -p 1*5+20*1+1*5 -o simSc_neutral_demoConstant_rep${i}_22params.msmc VcfSplit/Rep$i/simSc_neutral_demoConstant*.multihetsep.msmc
done
```

```r
lst <- list()
for (i in 1:10) {
  d <- NULL
  try(d <- read.table(paste0("simSc_neutral_demoConstant_rep", i, "_22params.msmc.final.txt"), header = TRUE))
  if (!is.null(d)) {
    d$replicate <- i
    lst[[i]] <- d
  }
} #No failure!

library(data.table)
dat <- rbindlist(lst)

library(ggplot2)
p <- ggplot(dat) + geom_step(aes(x = left_time_boundary, y = 1/lambda, group = replicate)) +
  scale_x_log10() + scale_y_log10()
p
```
Much better!

Now we add missing data, in the form of a callability mask. We use the mask of the FarEastAsian population as an example.

```r
dat <- read.table("../AlignmentProcessing/FarEastAsian/FarEastAsian_R64.statistics6.csv", header = TRUE)
dat <- dat[,1:3]
chrs <- read.csv("Chromosomes.csv")
for(j in 1:nrow(chrs)) {
  cat("- Chr ", chrs[j, "chr"], "...\n")
  bed <- subset(dat, Chr == chrs[j, "chr"]);
  bed[,2] <- bed[,2] + chrs[j, "begin"] - 1
  bed[,3] <- bed[,3] + chrs[j, "begin"] - 1
  write.table(bed, file = paste0("Mask_chr", chrs[j, "chr"], ".bed"), sep = "\t", col.names = FALSE, row.names = FALSE, quote = FALSE)
}
```

```r
chrs <- read.csv("Chromosomes.csv")
for (i in 1:10) {
  cat("Rep ", i, "...\n")
  for(j in 1:nrow(chrs)) {
    cat("- Chr ", chrs[j, "chr"], "...\n")
    system(paste0("python3 generate_multihetsep.py --chr ", chrs[j, "chr"], " --mask Mask_chr", chrs[j, "chr"], ".bed VcfSplit/Rep", i, "/Chr", chrs[j, "chr"], "/simSc_neutral_demoConstant_rep", i, ".indv*.vcf.gz > VcfSplit/Rep", i, "/simSc_neutral_demoConstant_masked_rep", i, ".chr", chrs[j, "chr"], ".multihetsep.msmc"))
  }
}
```

Run MSMC2:
```bash
for i in {1..10}; do
  echo "Rep $i..."
  msmc2 -p 1*5+20*1+1*5 -o simSc_neutral_demoConstant_masked_rep${i}_22params.msmc VcfSplit/Rep$i/simSc_neutral_demoConstant_masked*.multihetsep.msmc
done
```
 

Simulations with background selection
=====================================

Run SLIM on a SLURM cluster (10 replicates):
```bash
sbatch runSLIM_constant.sh
sbatch runSLIM_decline.sh
```
Note: this creates very large treesequences. We do not add them to the git repository.

Parenthesis: compute the proportion of the genome that codes for proteins:
```r
sum(exons$width) / sum(chromosomes$end-chromosomes$start)
```
=> 73.58092%

Now recapitate and sample the tree sequence:
```
tskit = 0.4.0
pylim = 0.700
msprime = 1.1.0
```

```bash
sbatch runRecapitate_constant.sh
sbatch runRecapitate_decline.sh
```

Then add mutations. This is fast and can run on a laptop:

```bash
for i in {1..10}; do
  echo "Rep $i..."
  python3.9 addNeutralMutations.py simSc_demoConstant_rep$i $((42 + i)) > simSc_demoConstant_rep${i}_msprime_mutation.log
done
```
For the decreasing pop size too:
```bash
for i in {1..10}; do
  echo "Rep $i..."
  python3.9 addNeutralMutations.py simSc_demoDecline_rep$i $((42 + i)) > simSc_demoDecline_rep${i}_msprime_mutation.log
done
```

Cut into individual chromosomes:
```r
chrs <- read.csv("Chromosomes.csv")
dir.create("VcfSplit")
for (i in 1:10) {
  cat("Rep ", i, "...\n")
  dir.create(paste0("VcfSplit/Rep", i))
  for(j in 1:nrow(chrs)) {
    cat("- Chr ", chrs[j, "chr"], "...\n")
    dir.create(paste0("VcfSplit/Rep", i, "/Chr", chrs[j, "chr"]))
    for (k in 0:4) {
      cat("  + Individual ", k, "...\n")
      system(paste0("vcftools --gzvcf simSc_demoConstant_rep", i, ".5indv.vcf.gz --chr 1 --indv tsk_", k, " --from-bp ", chrs[j, "begin"], " --to-bp ", chrs[j, "end"], " --recode --stdout | bgzip -c > VcfSplit/Rep", i, "/Chr", chrs[j, "chr"], "/simSc_demoConstant_rep", i, ".indv", k, ".chr", chrs[j, "chr"], ".vcf.gz"))
    }
  }
}
# Now for the decreasing pop size:
for (i in 1:10) {
  cat("Rep ", i, "...\n")
  for(j in 1:nrow(chrs)) {
    cat("- Chr ", chrs[j, "chr"], "...\n")
    for (k in 0:4) {
      cat("  + Individual ", k, "...\n")
      system(paste0("vcftools --gzvcf simSc_demoDecline_rep", i, ".5indv.vcf.gz --chr 1 --indv tsk_", k, " --from-bp ", chrs[j, "begin"], " --to-bp ", chrs[j, "end"], " --recode --stdout | bgzip -c > VcfSplit/Rep", i, "/Chr", chrs[j, "chr"], "/simSc_demoDecline_rep", i, ".indv", k, ".chr", chrs[j, "chr"], ".vcf.gz"))
    }
  }
}
```

Then generate MSMC input files, using the FarEastAsian population as an example:
```r
chrs <- read.csv("Chromosomes.csv")
for (i in 1:10) {
  cat("Rep ", i, "...\n")
  for(j in 1:nrow(chrs)) {
    cat("- Chr ", chrs[j, "chr"], "...\n")
    system(paste0("python3 generate_multihetsep.py --chr ", chrs[j, "chr"], " --mask Mask_chr", chrs[j, "chr"], ".bed VcfSplit/Rep", i, "/Chr", chrs[j, "chr"], "/simSc_demoConstant_rep", i, ".indv*.vcf.gz > VcfSplit/Rep", i, "/simSc_demoConstant_masked_rep", i, ".chr", chrs[j, "chr"], ".multihetsep.msmc"))
  }
}
# Now for the decreasing pop size:
for (i in 1:10) {
  cat("Rep ", i, "...\n")
  for(j in 1:nrow(chrs)) {
    cat("- Chr ", chrs[j, "chr"], "...\n")
    system(paste0("python3 generate_multihetsep.py --chr ", chrs[j, "chr"], " --mask Mask_chr", chrs[j, "chr"], ".bed VcfSplit/Rep", i, "/Chr", chrs[j, "chr"], "/simSc_demoDecline_rep", i, ".indv*.vcf.gz > VcfSplit/Rep", i, "/simSc_demoDecline_masked_rep", i, ".chr", chrs[j, "chr"], ".multihetsep.msmc"))
  }
}
```

Finally run MSMC2:
```bash
for i in {1..10}; do
  echo "Rep $i..."
  msmc2 -p 1*5+20*1+1*5 -o simSc_demoConstant_masked_rep${i}_22params.msmc VcfSplit/Rep$i/simSc_demoConstant_masked*.multihetsep.msmc
done
```
And for the decreasing pop size:
```bash
for i in {1..10}; do
  echo "Rep $i..."
  msmc2 -p 1*5+20*1+1*5 -o simSc_demoDecline_masked_rep${i}_22params.msmc VcfSplit/Rep$i/simSc_demoDecline_masked*.multihetsep.msmc
done
```

