#!/bin/bash

#SBATCH --job-name=yeast-slim-constant
#SBATCH --ntasks=1 # num of cores
#SBATCH --nodes=1
#SBATCH --time=300:00:00 # in hours
#SBATCH --mem=500G 
#SBATCH --error=log/yeast_slim_constant.%J.err
#SBATCH --output=log/yeast_slim_constant.%J.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=dutheil@evolbio.mpg.de
#SBATCH --partition=global 
#SBATCH --array=1-10

~/.local/bin/slim -d seed=$((SLURM_ARRAY_TASK_ID + 42)) -d rep=$SLURM_ARRAY_TASK_ID SimulateConstant.slim
  
