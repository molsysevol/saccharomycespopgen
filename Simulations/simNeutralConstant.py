#! /usr/bin/python3

import msprime, tskit, numpy, sys, random, gzip
  
# Default values:
u = 1.7e-7
r = 1e-7
n = 5 
L = int(12071326)
N = 10000
seed = 42
n_rep = 10

if __name__ == "__main__":
    seed = int(sys.argv[1])
    print("Random seed: %i\n" % seed)
    numpy.random.seed(seed)

    demography = msprime.Demography()
    demography.add_population(name = "Pop", initial_size = N)

    dd = msprime.DemographyDebugger(demography = demography)
  
    mutation_model = msprime.InfiniteSites(msprime.NUCLEOTIDES)
  
    tss = msprime.sim_ancestry(
      samples = {"Pop": n},
      demography = demography,
      sequence_length = L,
      discrete_genome = True,
      recombination_rate = r,
      model = "hudson",
      num_replicates = n_rep,
      random_seed = seed)
    
    for i, ts in enumerate(tss):
        print("  * Simulating replicate %i" % i)
        tsm = msprime.mutate(ts, rate = u, model = mutation_model, random_seed = seed)
    
        output = "simSc_neutral_demoConstant_rep" + str(i) + ".5indv.vcf.gz"
        with gzip.open(output, "wt") as vcf_file:
            tsm.write_vcf(vcf_file, position_transform = "legacy") # 'legacy' is used to avoid SNPs at the same position

