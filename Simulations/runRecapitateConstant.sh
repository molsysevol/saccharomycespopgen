#!/bin/bash

#SBATCH --job-name=yeast-recapitate-constant
#SBATCH --ntasks=1 # num of cores
#SBATCH --nodes=1
#SBATCH --time=300:00:00 # in hours
#SBATCH --mem=100G 
#SBATCH --error=log/yeast_recapitate_constant.%J.err
#SBATCH --output=log/yeast_recapitate_constant.%J.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=dutheil@evolbio.mpg.de
#SBATCH --partition=global 
#SBATCH --array=1-10

module load python/3.9.13
python3.9 recapitateAndSample.py simSc_demoConstant_rep${SLURM_ARRAY_TASK_ID} $((SLURM_ARRAY_TASK_ID + 42)) > simSc_demoConstant_rep${SLURM_ARRAY_TASK_ID}_msprime_recapitation.log
  
